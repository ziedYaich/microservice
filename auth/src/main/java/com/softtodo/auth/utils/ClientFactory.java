package com.softtodo.auth.utils;

import com.softtodo.auth.entities.Client;
import com.softtodo.auth.entities.Scope;
import com.softtodo.auth.entities.Role;
import com.softtodo.auth.repository.ScopeRepository;
import com.softtodo.auth.repository.RoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ClientFactory {
    @Autowired
    RoleRepository roleRepository;
    @Autowired
    ScopeRepository scopeRepository;

    private Client createClient(String clientId, String secret, String name, String description, String role, String scope) {
        Client client = new Client();
        client.setName(name);
        if (clientId != null) {
            client.setClientId(clientId);
        }
        if (secret != null) {
            client.setSecret(secret);
        }
        client.setDescription(description);
        client.setScoped(false);
        Role newRole = roleRepository.findByName(role);
        if (newRole == null) {
            newRole = new Role();
            newRole.setName(role);
            newRole = roleRepository.save(newRole);
        }
        client.getRoles().add(newRole);
        Scope newScope = scopeRepository.findByName(scope);
        if (newScope == null) {
            newScope = new Scope();
            newScope.setName(scope);
            newScope = scopeRepository.save(newScope);
        }
        client.getScopes().add(newScope);
        return client;
    }

    public Client server(String name, String description) {
        Client client = createClient("server", "server", name, description, "ROLE_SERVER", "server");
        client.setType(Client.ClientType.SERVER);
        return client;
    }

    public Client userGuest(String name, String description, String redirectUri, String clientId, String secret) {
        Client client = createClient(clientId, secret, name, description, "ROLE_GUEST_USER", "client");
        client.setRedirectUri(redirectUri);
        client.setType(Client.ClientType.USER_GUEST);
        return client;
    }

    public Client clientGuest(String name, String description, String role, String clientId, String secret) {
        Client client = createClient(clientId, secret, name, description, role, "client");
        client.setType(Client.ClientType.CLIENT_GUEST);
        return client;
    }
}
