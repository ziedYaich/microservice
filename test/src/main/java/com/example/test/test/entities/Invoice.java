package com.example.test.test.entities;

import com.example.test.BaseEntity;
import com.example.test.annotations.ContentData;
import com.example.test.annotations.ModifiableContentAssociation;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import java.util.List;
@Entity
public class Invoice extends BaseEntity {
    @ManyToOne(fetch = FetchType.EAGER)
    private Client client;


    @ContentData
    @OneToMany(mappedBy = "invoice",fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private List<InvoiceLine> invoiceLines;


    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public List<InvoiceLine> getInvoiceLines() {
        return invoiceLines;
    }

    public void setInvoiceLines(List<InvoiceLine> invoiceLines) {
        this.invoiceLines = invoiceLines;
    }
}
