import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";

import {TranslateService} from "@ngx-translate/core";
@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  form: FormGroup;

  constructor( private translate: TranslateService,private fb: FormBuilder) {
      this.form = fb.group({
        'FirstName' : [null, Validators.required],
        'LastName' : [null, Validators.required],
        'Date' : [null, Validators.required]
      });
  }

  /* Notify Action Item */
  EditProfile(form){
    console.log(form);

  }


  ngOnInit() {
  }

}
