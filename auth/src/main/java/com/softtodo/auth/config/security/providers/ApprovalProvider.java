package com.softtodo.auth.config.security.providers;

import com.softtodo.auth.entities.Approve;
import com.softtodo.auth.entities.Client;
import com.softtodo.auth.entities.User;
import com.softtodo.auth.repository.ApprovalRepository;
import com.softtodo.auth.repository.ClientRepository;
import com.softtodo.auth.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.oauth2.provider.approval.Approval;
import org.springframework.security.oauth2.provider.approval.ApprovalStore;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Component
public class ApprovalProvider implements ApprovalStore {

    @Autowired
    private ApprovalRepository approvalRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private ClientRepository clientRepository;

    @Override
    public boolean addApprovals(Collection<Approval> approvals) {

        return approvals.stream().map(approval -> {
            Approve approve = approveFromApproval(approval);
            if (approve.getId() != null) {
                boolean change = false;
                if (notEquals(approval.getStatus(), approve.getStatus())) {
                    approve.setStatus(approval.getStatus());
                    change = true;
                }
                if (notEquals(approval.getLastUpdatedAt(), approve.getLastUpdatedAt())) {
                    approve.setLastUpdatedAt(approval.getLastUpdatedAt());
                    change = true;
                }
                if (change) {
                    approvalRepository.save(approve);
                    return true;
                } else {
                    return false;
                }
            }
            approvalRepository.save(approve);
            return true;
        }).reduce((res, current) -> res && current).orElse(true);
    }


    @Override
    public boolean revokeApprovals(Collection<Approval> approvals) {
        return approvals.stream().map(approval -> {
            Approve approve = approveFromApproval(approval);
            if (approve != null) {
                approvalRepository.delete(approve);
                return true;
            }
            return false;
        }).reduce((res, current) -> res && current).orElse(true);

    }

    @Override
    public Collection<Approval> getApprovals(String userId, String clientId) {
        //List<Approve> approves = approvalRepository.findApproves(clientId, userId);
        //if (approves == null)
            return Collections.emptySet();
        /*return approves.stream().map(approve -> new Approval(approve.getUser().getUsername(),
                approve.getClient().getClientId(),
                approve.getScope(),
                approve.getExpiresAt(),
                approve.getStatus(),
                approve.getLastUpdatedAt())).collect(Collectors.toList());*/
    }

    private static boolean notEquals(Object a, Object b) {
        return a == null && b == null || !Objects.equals(a, b);
    }

    private Approve approveFromApproval(Approval approval) {
        User user = userRepository.findByUsername(approval.getUserId());
        Client client = clientRepository.findOne(approval.getClientId());
        Approve approve = approvalRepository.findByClientAndUserAndScope(client, user, approval.getScope());
        return approve == null ? new Approve(approval, user, client) : approve;

    }
}