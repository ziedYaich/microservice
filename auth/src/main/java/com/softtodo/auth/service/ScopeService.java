package com.softtodo.auth.service;

import com.softtodo.auth.entities.Scope;
import com.softtodo.core.response.ResponseException;
import com.softtodo.core.services.MongoGenericService;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.io.IOException;

@Service
public class ScopeService extends MongoGenericService<Scope> {
    public Scope findByName(String name) {
        return findOneBy("name", name);
    }
}
