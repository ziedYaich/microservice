package com.softtodo.auth.service;

import com.softtodo.auth.entities.Approve;
import com.softtodo.auth.entities.Client;
import com.softtodo.auth.entities.User;
import com.softtodo.core.services.MongoGenericService;
import org.springframework.stereotype.Service;

@Service
public class ApproveService extends MongoGenericService<Approve> {
    public Approve findByClientAndUserAndScope(Client client, User user, String scope){
        return findOneBy("client,user,scope",client, user, scope);
    }
}
