package com.softtodo.core.entities.annotations;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Specifies the validation regex expression of this field.
 *
 */
@Documented
@Target({ElementType.FIELD,ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface Validation {
    String regex() default "";
    String minLength() default "";
    String maxLength() default "";
    String min() default "";
    String max() default "";
}
