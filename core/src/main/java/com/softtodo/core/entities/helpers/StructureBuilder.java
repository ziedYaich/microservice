package com.softtodo.core.entities.helpers;

import com.softtodo.core.entities.BaseEntity;
import org.reflections.Reflections;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class StructureBuilder {

    private static Map<Class<? extends BaseEntity>, EntityStructure<? extends BaseEntity>> structureMap = new HashMap<>();

    public static <T extends BaseEntity> EntityStructure<T> getClassStructure(Class<T> clazz) {
        @SuppressWarnings("unchecked")
        EntityStructure<T> structure = (EntityStructure<T>) structureMap.get(clazz);
        if (structure == null) {
            structure = new EntityStructure<>(clazz);
            structureMap.put(clazz,structure);
        }
        return structure;
    }

}
