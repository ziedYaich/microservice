import {AfterViewInit, Component, Input, OnInit, ViewChild} from '@angular/core';
import {MatDialog, MatPaginator, MatSort, MatTableDataSource} from "@angular/material";
import { trigger, style, transition, animate, keyframes, query, stagger, state, useAnimation,
  AnimationOptions } from '@angular/animations';
import { bounceIn,bounceOut,zoomIn} from 'ngx-animate';
import {ActivatedRoute, Router} from "@angular/router";
import {ModalRemove} from "../../shared/GenericCRUD/genericDialogRemove/modalRemove.component";


@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css'],
  animations: [
      trigger('triggerfadeIn', [
        transition( '* => *', useAnimation(bounceIn, {params : {timing: 0.1}}))
      ]),
      trigger('zoomIn', [
        transition( '* => *', useAnimation(zoomIn, {params : {timing: 0.2}}))
      ])
  ]
})
export class UserComponent implements AfterViewInit,OnInit{


    /* Set a Structure Fields To Generic Add */
       structureFields:any = Structure_Add;
    /* Set a data To dataTable*/
       dataSource:any = Directive_DATA;
    /* Set a title Component To Generic Add*/
       titleComponent:any;
    /* Set a itemDetail To Generic Detail*/
       itemDetail:any;
    /* Set a Data Detail To Generic Detail*/
       dataEdit:any;

       rootview:any;

      constructor(public dialog: MatDialog,private  router : Router ,private route: ActivatedRoute) {

      }

      ngOnInit(): void {
        this.titleComponent=this.structureFields.name+".add";
        this.route.paramMap.subscribe(params => {
          this.rootview = params.get('rootview');
          if(this.rootview !='add' && this.rootview !='list' && this.rootview !='edit' && this.rootview !='detail')
          { this.router.navigate(['/error/404']); }
        });
      }

      ngAfterViewInit() {
      }

      /* Add User */
      addUser(form:any): void {
        console.log(form);
      }


      /* Notify Action User */
      NotifyAction(eventNotify:any){
            if(eventNotify[1]=='add')
            {
              this.titleComponent=this.structureFields.name+".add";
              this.rootview='add';
              this.dataEdit=[];
              this.router.navigate(['user/add']);
            }
            else if(eventNotify[1]=='edit')
            {
              this.EditUser(eventNotify[0]);
              this.titleComponent=this.structureFields.name+".edit";
              this.rootview='add';
              this.dataEdit=eventNotify[0];
              this.router.navigate(['user/edit']);
            }
            else if(eventNotify[1]=='detail')
            {
              this.rootview='detail';
              this.itemDetail=eventNotify[0];
              this.router.navigate(['user/detail']);
            }
            else if(eventNotify[1]=='remove')
            {
              this.removeUser(eventNotify[0]);
            }

      }

        /* Remove User */
        removeUser(user:any)  {
          console.log(user);
          let dialogRef = this.dialog.open(ModalRemove,
              { height: 'auto', width: '600px'  , data: { itemRemove: 'User'} });

          const sub = dialogRef.componentInstance.RemoveAction.subscribe((data) => {
            console.log(data);
          });
          dialogRef.afterClosed().subscribe(result => {
            console.log('The dialog was closed');
          });
        }

        /* Edit User */
        EditUser(user:any):void {
          console.log(user);

        }

}

const Directive_DATA:any = [
  { "Picture":"assets/images/users/4.jpg",
    "Nom": "yessine",
    "Description": "abid",
    "Email":"yessine.abid@live.fr",
    "Date": "2018-02-26T00:00:00",
    "Authorization":true
  },
  { "Picture":"assets/images/users/5.jpg",
    "Nom": "zied",
    "Description": "yaich 5",
    "Email":"zied.yaich5@gmail.com",
    "Date": "2018-02-12T00:00:00",
    "Authorization":false
  }
  ,
  {
    "Nom": "khaled",
    "Description": "baklouti",
    "Email":"khaledbaklouti8@gmail.com",
    "Date": "2018-02-26T00:00:00"
  }
  ,
  { "Picture":"assets/images/users/d5.jpg",
    "Nom": "yessine",
    "Description": "abid",
    "Email":"yessine.abid@live.fr",
    "Date": "2018-02-26T00:00:00"
  }
  ,
  { "Picture":"assets/images/users/d5.jpg",
    "Nom": "yessine",
    "Description": "abid",
    "Email":"yessine.abid@live.fr",
    "Date": "2018-02-26T00:00:00"
  }
  ,
  { "Picture":"assets/images/users/d5.jpg",
    "Nom": "yessine",
    "Description": "abid",
    "Email":"yessine.abid@live.fr",
    "Date": "2018-02-26T00:00:00"
  }
  ,
  { "Picture":"assets/images/users/d5.jpg",
    "Nom": "yessine",
    "Description": "abid",
    "Email":"yessine.abid@live.fr",
    "Date": "2018-02-26T00:00:00"
  }
  ,
  { "Picture":"assets/images/users/d5.jpg",
    "Nom": "yessine",
    "Description": "abid",
    "Email":"yessine.abid@live.fr",
    "Date": "2018-02-26T00:00:00"
  }


];




const Structure_Add:any =
  {
    "name":"user",
    "displaySingularName":"User",
    "displayPluralName":"Users",
    "fields":[

      {
        "name":"Picture",
        "displayName":"Picture",
        "type":{
          "name":"picture",
          "editable":true,
          "src":"assets/images/users/defaultimg.png",
          "showDataTabel":true
        }
      },
      {
        "name":"Nom",
        "displayName":"Nom",
        "type":{
          "name":"string",
          "required":true,
          "editable":true,
          "minLength":3,
          "maxLength":20,
          "showDataTabel":true
        }
      },
      {
        "name":"Email",
        "displayName":"Email",
        "type":{
          "name":"email",
          "required":true,
          "editable":true,
          "minLength":3,
          "maxLength":20,
          "showDataTabel":true
        }
      },
      {
        "name":"Date",
        "displayName":"Date",
        "type":{
          "name":"date",
          "required":true,
          "editable":true,
          "showDataTabel":true
        }
      },
      {
        "name":"Authorization",
        "displayName":"Authorization",
        "type":{
            "name":"toggle",
            "required":true ,
            "editable":true,
            "showDataTabel":false
        }
      },

      {
        "name":"Description",
        "displayName":"Description",
        "type":{
          "name":"Textarea",
          "required":true,
          "editable":true,
          "minLength":3,
          "maxLength":20,
          "showDataTabel":true
        }
      }
    ]
  }
;
