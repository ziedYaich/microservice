package com.softtodo.auth.controller;

import com.softtodo.auth.entities.Client;
import com.softtodo.core.controllers.MongoGenericController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("client")
public class ClientController extends MongoGenericController<Client> {
}
