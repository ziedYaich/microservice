package com.softtodo.auth.repository;

import com.softtodo.auth.entities.Scope;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.List;

@Repository
public interface ScopeRepository extends MongoRepository<Scope, String> {
    Scope findByName(String scope);
    List<Scope> findByNameIn(Collection<String> names);
}
