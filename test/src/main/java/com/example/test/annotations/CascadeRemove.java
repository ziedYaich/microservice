package com.example.test.annotations;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * Specifies that this association will be really deleted
 * default delete is making the deleted attribute true
 * @since Java Persistence 1.0
 */
@Documented
@Target({FIELD})
@Retention(RUNTIME)
public @interface CascadeRemove {

}
