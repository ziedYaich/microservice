package com.softtodo.auth.service;

import com.softtodo.auth.entities.Role;
import com.softtodo.core.services.MongoGenericService;
import org.springframework.stereotype.Service;

@Service
public class RoleService extends MongoGenericService<Role> {
}
