package com.softtodo.core.services;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.softtodo.core.entities.BaseEntity;
import com.softtodo.core.entities.helpers.EntityStructure;
import com.softtodo.core.entities.helpers.StructureBuilder;
import com.softtodo.core.response.ResponseException;
import com.softtodo.core.search.SearchObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.InvalidMongoDbApiUsageException;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.data.mongodb.repository.query.MongoEntityInformation;
import org.springframework.data.mongodb.repository.support.MongoRepositoryFactory;
import org.springframework.data.util.ClassTypeInformation;
import org.springframework.data.util.TypeInformation;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.util.Assert;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.data.mongodb.core.query.Criteria.where;


/**
 * the mongo generic service implementation,
 * This is an implementation for common database operations to manage all documents management.
 *
 * @author Khaled Baklouti
 */
public class MongoGenericService<ENTITY extends BaseEntity> {
    @Autowired
    private MongoOperations mongoOperations;
    private ObjectMapper objectMapper;

    private EntityStructure<ENTITY> entityStructure;
    private MongoEntityInformation<ENTITY, String> entityInformation;
    private Class<ENTITY> entityClass;

    public final MongoOperations getMongoOperations() {
        return mongoOperations;
    }


    @PostConstruct
    @SuppressWarnings("unchecked")
    private void construct() {
        this.entityClass = resolveEntityClass();
        this.entityInformation = new MongoRepositoryFactory(mongoOperations).getEntityInformation(entityClass);
        this.entityStructure = StructureBuilder.getClassStructure(entityClass);
        this.objectMapper = new ObjectMapper();
        SimpleModule module = new SimpleModule(entityClass.getSimpleName());
        module.addDeserializer(entityClass, new EntityDeserializer());
        objectMapper.registerModule(module);
    }

    /**
     * Insert the document in the database.
     * Insert is used to initially store the object into the database.
     * To update an existing object use the update method.
     *
     * @param entity the object to store in the collection
     */
    public final ENTITY insert(ENTITY entity) {
        Assert.notNull(entity, "Entity must not be null!");
        Assert.isNull(entity.getId(), "Cannot insert an entity with an id");
        entity = createChanges(entity);
        mongoOperations.save(entity, entityInformation.getCollectionName());
        return entity;
    }

    /**
     * Update the object to the collection for the entity type of the object to save.
     *
     * @param entity the object to be updated in the collection
     */
    public final ENTITY update(ENTITY entity) {
        Assert.notNull(entity, "Entity must not be null!");
        Assert.notNull(entity.getId(), "Cannot update an entity without its id");
        entity = createChanges(entity);
        mongoOperations.save(entity, entityInformation.getCollectionName());
        return entity;
    }

    /**
     * Update the object to the collection for the entity type of the object to save.
     * Use this method if you only need to update the fields in the json.
     *
     * @param json the json string of the object to be updated in the collection
     */
    public final ENTITY update(String json) {
        Assert.hasText(json, "Invalid Json (empty or null)");
        ENTITY entity = createChanges(json);
        Assert.notNull(json, "Invalid Json");
        Assert.notNull(entity.getId(), "Cannot insert an entity without its id");
        mongoOperations.save(entity, entityInformation.getCollectionName());
        return entity;
    }

    /**
     * Update the object to the collection for the entity type of the object to save.
     *
     * @param id     the id of the document to be updated in the collection
     * @param entity the object to be updated in the collection
     */
    public final ENTITY update(String id, ENTITY entity) {
        entity.setId(id);
        return update(entity);
    }

    /**
     * Update the object to the collection for the entity type of the object to save.
     * Use this method if you only need to update the fields in the json.
     *
     * @param id   the id of the document to be updated in the collection
     * @param json the json string of the object to be updated in the collection
     */
    public final ENTITY update(String id, String json) {
        ENTITY entity = createChanges(json);
        Assert.notNull(entity, "Invalid Json");
        Assert.notNull(entity.getId(), "Cannot insert an entity without its id");
        entity.setId(id);
        mongoOperations.save(entity, entityInformation.getCollectionName());
        return entity;
    }

    /**
     * Returns a document with the given id mapped onto the given class.
     *
     * @param id the id of the document to return.
     * @return the document with the given id mapped onto the given target class.
     */
    public final ENTITY findOne(String id) {
        Assert.notNull(id, "The given id must not be null!");
        ENTITY entity = mongoOperations.findById(id, entityInformation.getJavaType(), entityInformation.getCollectionName());
        if (entity == null || entity.isDeleted()) {
            return null;
        }
        filterDeleted(entity);
        return entity;
    }

    /**
     * Returns the number of documents for the given collection.
     *
     * @return the number of documents in this collection
     */
    public final long count() {
        return mongoOperations.getCollection(entityInformation.getCollectionName()).count(filterDeletedQuery(new Query()).getQueryObject());
    }

    /**
     * Returns the number of documents for the given collection matching the query.
     *
     * @return the number of documents in this collection matching the query
     */
    protected final long count(Query query) {
        return mongoOperations.getCollection(entityInformation.getCollectionName()).count(filterDeletedQuery(query).getQueryObject());
    }

    /**
     * Returns the number of documents for the given collection matching the searchObject.
     *
     * @return the number of documents in this collection matching the query
     */
    public final long count(SearchObject searchObject) {
        return count(filterDeletedQuery(new Query().addCriteria(searchObject.toCriteria())));
    }

    /**
     * return a List of all documents in the collection
     *
     * @return all documents in the collection
     */
    public final List<ENTITY> findAll() {
        return findAll(new Query());
    }


    /**
     * return a Page from all documents in the collection
     *
     * @param pageable the Page options
     * @return a Page from all documents in the collection
     */

    public final Page<ENTITY> findAll(final Pageable pageable) {
        return findAll(new Query(), pageable);
    }

    /**
     * return all documents in the collection which match the search option
     *
     * @return all documents in the collection which match the search option
     */
    public List<ENTITY> findAll(SearchObject searchObject) {
        return findAll(new Query().addCriteria(searchObject.toCriteria()));
    }

    /**
     * @return the first document in the collection which have the same value of the specified key
     */
    public final ENTITY findOneBy(String key, Object... values) {
        return findAllBy(key, values).stream().findFirst().orElse(null);
    }

    /**
     * @return the all documents in the collection which have the same value of the specified key
     */
    public final List<ENTITY> findAllBy(String key, Object... values) {
        String keys[] = key.split(",");
        if (keys.length != values.length) {
            throw new IllegalArgumentException("number of keys is different of number of values");
        }
        Criteria criteria[] = new Criteria[keys.length];
        for (int i = 0; i < keys.length; i++) {
            try {
                entityStructure.getFieldStructure(keys[i]);
            } catch (ResponseException e) {
                throw new IllegalArgumentException(e.getMessage());
            }
            criteria[i] = Criteria.where(keys[i]).is(values[i]);
        }
        return findAll(new Query().addCriteria(new Criteria().andOperator(criteria)));
    }

    /**
     * return a Page from all documents in the collection which match the search option
     *
     * @return return a Page from all documents in the collection which match the search option
     */
    public final Page<ENTITY> findAll(SearchObject searchObject, Pageable pageable) {
        Assert.notNull(searchObject, "null search Object");
        return findAll(new Query().addCriteria(searchObject.toCriteria()).with(pageable), pageable);
    }


    /**
     * deletes the document with the given id.
     * this operation is a soft delete it will set the field deleted of this document to true.
     * this document will stay persistent in the database but it will not be returned with the service.
     */
    public final void delete(String id) {
        ENTITY entity = findOne(id);
        if (entity == null) {
            throw new ResponseException(String.format("No entity with the given id : %s", id), 400);
        }
        if (entityStructure.canDelete(entity)) {
            mongoOperations.findAndModify(new Query().addCriteria(getIdCriteria(id)), new Update().set("deleted", true), entityClass);
            return;
        }
        throw new SecurityException("");
    }


    /**
     * deletes the document with the given id completely from the database.
     * Be careful this operation in irreversible
     */
    protected final void deleteCompletely(String id) {
        ENTITY entity = findOne(id);
        if (entity == null) {
            throw new ResponseException(String.format("No entity with the given id : %s", id), 400);
        }
        if (entityStructure.canDeleteCompletely(entity)) {
            mongoOperations.findAndRemove(new Query().addCriteria(getIdCriteria(id)), entityClass);
            return;
        }
        throw new SecurityException("");
    }

    /**
     * deletes all documents with the given ids.
     * this operation is a soft delete it will set the field deleted of documents to true.
     * this document will stay persistent in the database but it will not be returned with the service.
     */
    public final void delete(Collection<String> ids) {
        boolean canAccess = ids.stream().allMatch(id -> {
            ENTITY entity = findOne(id);
            if (entity == null) {
                throw new ResponseException(String.format("No entity with the given id : %s", id), 400);
            }
            return entityStructure.canDelete(entity);
        });
        if (canAccess) {
            mongoOperations.findAndModify(new Query().addCriteria(getIdsCriteria(ids)), new Update().set("deleted", true), entityClass);
            return;
        }
        throw new SecurityException("");
    }

    /**
     * deletes all documents with the given ids completely from the database.
     * Be careful this operation in irreversible
     */
    protected final void deleteCompletely(Collection<String> ids) {
        boolean canAccess = ids.stream().allMatch(id -> {
            ENTITY entity = findOne(id);
            if (entity == null) {
                throw new ResponseException(String.format("No entity with the given id : %s", id), 400);
            }
            return entityStructure.canDeleteCompletely(entity);
        });
        if (canAccess) {
            mongoOperations.findAndRemove(new Query().addCriteria(getIdsCriteria(ids)), entityClass);
            return;
        }
        throw new SecurityException("");
    }

    /**
     * @return the javaType of the document managed by this service.
     */
    public final Class<ENTITY> getEntityClass() {
        return entityClass;
    }

    /**
     * @return the structure of the document managed by this service.
     */
    public final EntityStructure<ENTITY> getEntityStructure() {
        return entityStructure;
    }

    @PreAuthorize("hasRole('ADMIN')")
    protected final List<ENTITY> findAll(Query query) {
        Assert.notNull(query, "null query");
        List<ENTITY> list = mongoOperations.find(filterDeletedQuery(query), entityInformation.getJavaType(), entityInformation.getCollectionName());
        list.forEach(this::filterDeleted);
        return list;
    }


    protected final Page<ENTITY> findAll(Query query, Pageable pageable) {
        Long count = count(query);
        List<ENTITY> list = findAll(query.with(pageable));
        return new PageImpl<>(list, pageable, count);
    }


    private final Criteria getIdCriteria(String id) {
        return new Criteria().andOperator(getNotDeletedCriteria(), where(entityInformation.getIdAttribute()).is(id));
    }

    private final Criteria getIdsCriteria(Collection<String> ids) {
        return new Criteria().andOperator(getNotDeletedCriteria(), where(entityInformation.getIdAttribute()).in(ids));
    }

    private final Criteria getNotDeletedCriteria() {
        return Criteria.where("deleted").is(false);
    }

    private final Query filterDeletedQuery(Query query) {
        try {
            query.addCriteria(getNotDeletedCriteria());
        } catch (InvalidMongoDbApiUsageException e) {
            if (!e.getMessage().contains("you can't add a second 'deleted' criteria.")) {
                throw e;
            }
        }
        return query;
    }


    @SuppressWarnings("unchecked")
    private final ENTITY createChanges(ENTITY entity) {
        Assert.notNull(entity, "Entity must not be null!");
        ENTITY oldEntity = entityInformation.isNew(entity) ? entityStructure.newInstance() : findOne(entity.getId());
        Assert.notNull(entity, "Entity id is not a valid id");
        entityStructure.forEach(fieldStructure -> {
            fieldStructure.setValue(oldEntity, fieldStructure.getValue(entity));
        });
        return oldEntity;
    }

    @SuppressWarnings("unchecked")
    private final void filterDeleted(BaseEntity entity) {
        entityStructure.forEach(fieldStructure -> {
            if (BaseEntity.class.isAssignableFrom(fieldStructure.getJavaType())) {
                if (((BaseEntity) fieldStructure.getValueSafely(entity)).isDeleted()) {
                    fieldStructure.setValueOfInstance(entity, null);
                } else {
                    filterDeleted((BaseEntity) fieldStructure.getValueSafely(entity));
                }
                return;
            }
            if (Collection.class.isAssignableFrom(fieldStructure.getJavaType())) {
                List removed = (List) ((Collection) fieldStructure.getValueSafely(entity)).stream()
                        .filter(object -> {
                            try {
                                return ((BaseEntity) object).isDeleted();
                            } catch (ClassCastException e) {
                                return false;
                            }
                        })
                        .collect(Collectors.toList());
                ((Collection) fieldStructure.getValueSafely(entity)).removeAll(removed);
            }
        });
    }

    @SuppressWarnings("unchecked")
    private final ENTITY createChanges(String stringNode) {
        Assert.hasText(stringNode, "JSON node must not be null or empty!");
        try {
            return objectMapper.readValue(stringNode, entityClass);
        } catch (IOException e) {
            throw new ResponseException("Invalid JSON", 400);
        }
    }


    @SuppressWarnings("unchecked")
    private final Class<ENTITY> resolveEntityClass() {
        TypeInformation<?> information = ClassTypeInformation.from(getClass());
        List<TypeInformation<?>> arguments = information.getSuperTypeInformation(MongoGenericService.class).getTypeArguments();
        if (!arguments.isEmpty() && arguments.get(0) != null) {
            return ((TypeInformation) arguments.get(0)).getType();
        } else {
            throw new IllegalArgumentException(String.format("Could not resolve domain type of %s!", getClass()));
        }
    }

    private class EntityDeserializer extends StdDeserializer<ENTITY> {

        private EntityDeserializer() {
            super(entityClass);
        }

        @Override
        @SuppressWarnings("unchecked")
        public ENTITY deserialize(JsonParser p, DeserializationContext ctx) throws IOException {
            JsonNode node = p.getCodec().readTree(p);
            ENTITY entity;
            try {
                entity = node.has("id") ? findOne(node.get("id").asText()) : entityClass.newInstance();
            } catch (InstantiationException | IllegalAccessException e) {
                throw new RuntimeException(e);
            }
            getEntityStructure().forEach(fieldStructure -> {
                if (node.has((fieldStructure.getName()))) {
                    try {
                        fieldStructure.setValue(entity, MongoGenericServiceHolder.get(entityClass).objectMapper.readValue(node.get(fieldStructure.getName()).toString(), fieldStructure.getJavaType()));
                    } catch (IOException e) {
                        throw new ResponseException("Invalid Json", 400);
                    }
                }
            });
            return entity;
        }
    }
}
