package com.softtodo.core.controllers;

import com.softtodo.core.entities.BaseEntity;
import com.softtodo.core.entities.helpers.EntityStructure;
import com.softtodo.core.response.ResponseException;
import com.softtodo.core.search.SearchObject;
import com.softtodo.core.services.MongoGenericService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

public class MongoGenericController<ENTITY extends BaseEntity> {
    @Autowired
    private MongoGenericService<ENTITY> service;

    public MongoGenericService<ENTITY> getService() {
        return service;
    }

    /**
     * Retrieve the structure of the entity class
     *
     * @return the structure of the entity class
     */
    @ResponseBody
    @RequestMapping(value = "structure", method = RequestMethod.GET)
    public EntityStructure<ENTITY> getStructure() {
        return service.getEntityStructure();
    }

    /**
     * Retrieves an entity by its id.
     *
     * @param id must not be {@literal null}.
     * @return the entity with the given id or {@literal null} if none found
     */
    @ResponseBody
    @RequestMapping(value = "{id}", method = RequestMethod.GET)
    public ENTITY findOne(@PathVariable("id") String id) {
        ENTITY result = service.findOne(id);
        if (result == null) {
            throw new ResponseException(String.format("No entity with id %s",id),400);
        }
        return result;
    }

    /**
     * Returns a {@link Page} of documents meeting the paging restriction provided in the {@code Pageable} object. In case no match could be found, an empty
     * {@link Page} is returned.
     *
     * @param pageable paging options
     * @return a page of entities
     */
    @ResponseBody
    @RequestMapping(value = "", method = RequestMethod.GET)
    public Page<ENTITY> findAll(Pageable pageable) {
        return service.findAll(pageable);
    }

    /**
     * Returns a {@link Page} of documents matching the given {@link SearchObject}. In case no match could be found, an empty
     * {@link Page} is returned.
     *
     * @param searchObject cannot be {@literal null}.
     * @param pageable cannot be {@literal null}.
     * @return a {@link Page} of documents matching the given {@link SearchObject}.
     */
    @ResponseBody
    @RequestMapping(value = "", method = RequestMethod.POST)
    public Page<ENTITY> findAll(@RequestBody SearchObject searchObject, Pageable pageable) {
        return service.findAll(searchObject, pageable);
    }

    /**
     * Returns the number of documents available.
     *
     * @return the number of documents
     */
    @ResponseBody
    @RequestMapping(value = "count", method = RequestMethod.GET)
    public long count() {
        return service.count();
    }

    /**
     * Returns the number of documents matching the given {@link SearchObject}.
     *
     * @param searchObject the {@link SearchObject} to count documents for, cannot be {@literal null}.
     * @return the number of instances matching the {@link SearchObject}.
     */
    @ResponseBody
    @RequestMapping(value = "count", method = RequestMethod.POST)
    public long count(@RequestBody SearchObject searchObject) {
        return service.count(searchObject);
    }

    /**
     * Update a document using a json.
     *
     * @param id the id of the document which will be updated
     * @return the updated document
     */
    @ResponseBody
    @RequestMapping(value = "{id}", method = RequestMethod.PUT)
    public ENTITY update(@PathVariable("id") String id, @RequestBody String node) {
        return service.update(id, node);
    }

    /**
     * Saves a given JSON of an document.
     *
     * @param entity the document which will be saved
     * @return the saved document
     */
    @ResponseBody
    @RequestMapping(value = "", method = RequestMethod.PUT)
    public ENTITY save(@RequestBody ENTITY entity) {
        return service.insert(entity);
    }

    /**
     * Removes a given entity. (it is only a soft delete)
     *
     * @param id the id of the entity wish will be removed
     */
    @ResponseBody
    @RequestMapping(value = "{id}", method = RequestMethod.DELETE)
    public void delete(@PathVariable("id") String id) {
        service.delete(id);
    }
}
