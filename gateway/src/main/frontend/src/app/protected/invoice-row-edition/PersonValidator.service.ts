import {FormControl, FormGroup, Validators} from "@angular/forms";
import {Injectable} from "@angular/core";
import {ValidatorService} from "angular4-material-table";
import 'rxjs/add/operator/map'

@Injectable()
export class PersonService implements ValidatorService {
  getRowValidator(): FormGroup {
    return new FormGroup({
      'firstname': new FormControl(null, Validators.required),
      'lastname': new FormControl(null, Validators.required),
      'email': new FormControl(null, Validators.email),
      'age': new FormControl(null, Validators.required)
    });
  }
}
