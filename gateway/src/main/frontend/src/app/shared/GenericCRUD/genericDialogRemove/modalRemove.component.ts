import {AfterViewInit, Component, EventEmitter, Inject, Output, ViewChild} from '@angular/core';

import {  MatPaginator, MatSort, MatTableDataSource,
  MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import {FormBuilder, FormGroup, FormControl, Validators} from '@angular/forms';

@Component({
  selector: 'modal-remove',
  templateUrl: 'modal-Remove.html',
  styleUrls: []
})
export class ModalRemove {

      form: FormGroup;
      RemoveAction = new EventEmitter();

      item:any;

      constructor(
        public dialogRef: MatDialogRef<ModalRemove>,
        @Inject(MAT_DIALOG_DATA) public data: any,private fb: FormBuilder) {


        this.form = fb.group({
          'FirstName' : [null, Validators.required],
          'LastName' : [null, Validators.required],
          'Date' : [null, Validators.required]
        });


      }

      /* Notify Action Item */
      RemoveDataNotify(form){
        this.RemoveAction.emit(form);
      }


      onNoClick(): void {
        this.dialogRef.close();
      }


      addPost(user:any): void {
        console.log(user);
      }

}
