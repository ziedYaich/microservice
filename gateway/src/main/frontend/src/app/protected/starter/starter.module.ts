import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {CommonModule} from '@angular/common';
import {DemoMaterialModule} from '../../demo-material-module';
import {FlexLayoutModule} from '@angular/flex-layout';
import {  StarterComponent} from './starter.component';
import {StarterRoutes} from './starter.routing';

import { ChartsModule } from 'ng2-charts';
import {TranslateModule} from "@ngx-translate/core";
@NgModule({
  imports: [
    CommonModule,
    DemoMaterialModule,
    FlexLayoutModule,
    RouterModule.forChild(StarterRoutes),
    ChartsModule,
    TranslateModule
  ],
  entryComponents: [
  ],
  declarations: [ StarterComponent  ]
})

export class StarterModule {}
