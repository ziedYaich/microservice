import {Routes} from '@angular/router';

import {ExpansionComponent} from './expansion/expansion.component';
import {FullComponent} from "../layouts/full/full.component";
import {StarterComponent} from "./starter/starter.component";
import {UserComponent} from "./user/user.component";
import {UserModule} from "./user/user.module";
import {InvoiceComponent} from "./GenericInvoice/invoice.component";
import {InvoiceModule} from "./GenericInvoice/invoice.module";
import {ProfileModule} from "./profile/profile.module";
import {ProfileComponent} from "./profile/profile.component";
import {InvoiceRowEdition} from "./invoice-row-edition/invoice-row-edition.component";
import {NewInvoiceModule} from "./invoice-row-edition/invoice-row-edition.module";


export const ProtectedRoutes: Routes = [

      {
         path: '',
        loadChildren: './starter/starter.module#StarterModule'
      },
      {
        path: 'invoice',

        loadChildren: './GenericInvoice/invoice.module#InvoiceModule'

      },
      {
        path: 'profile',
        loadChildren: './profile/profile.module#ProfileModule'
      },
      {
        path: 'product',
        loadChildren: './product/product.module#ProductModule'
      },
      {
        path: 'user',
        loadChildren: './user/user.module#UserModule'
      } ,
      {
        path: 'expansion',
        component: ExpansionComponent
      } ,
      {
        path: 'newinvoice',
        loadChildren: './invoice-row-edition/invoice-row-edition.module#NewInvoiceModule'
      }



];
