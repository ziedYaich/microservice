package com.softtodo.auth.repository;

import com.softtodo.auth.entities.Approve;
import com.softtodo.auth.entities.Client;
import com.softtodo.auth.entities.User;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ApprovalRepository extends MongoRepository<Approve, String> {
    Approve findByClientAndUserAndScope(Client client, User user, String scope);
}
