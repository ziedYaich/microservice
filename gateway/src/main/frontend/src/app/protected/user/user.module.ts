import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule} from "@angular/router";
import {FlexLayoutModule} from "@angular/flex-layout";
import {UserRoutes} from "../user/user.routing";
import {DemoMaterialModule} from "../../demo-material-module";
import {UserComponent} from "./user.component";

import {FormsModule, FormGroup, ReactiveFormsModule} from '@angular/forms';

/* Generic Components */
import {GenericModule} from "../../shared/GenericCRUD/generic.module";
import {TranslateModule} from "@ngx-translate/core";



@NgModule({
  imports: [
    CommonModule,
    DemoMaterialModule,
    FlexLayoutModule,
    RouterModule.forChild(UserRoutes),
    FormsModule,
    ReactiveFormsModule,
    GenericModule,
    TranslateModule

  ],
  entryComponents: [

  ],
  declarations: [
    UserComponent


  ]
})
export class UserModule { }
