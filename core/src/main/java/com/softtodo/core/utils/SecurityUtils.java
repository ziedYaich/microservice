package com.softtodo.core.utils;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.provider.OAuth2Authentication;

public class SecurityUtils {

    public static OAuth2Authentication currentAuthentication() {
        return (OAuth2Authentication) SecurityContextHolder.getContext().getAuthentication();
    }

    public static String currentUsername() {
        return currentAuthentication().getUserAuthentication().getName();
    }

    public static String currentClientId() {
        return currentAuthentication().getOAuth2Request().getClientId();
    }
}
