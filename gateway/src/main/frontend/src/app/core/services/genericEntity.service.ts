import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import {GenericEntites} from "../entites/GenericEntites";

import {HttpClient, HttpHeaders} from '@angular/common/http';
import {UrlbaseService} from './urlbase.service';

const headers = new HttpHeaders().set("Content-Type", "application/json")
  .set( 'Accept','application/json' ).set("X-CustomHeader", "custom header value");

@Injectable()
export class GenericEntitesService {


  _apiUrl:any;

  constructor(private http: HttpClient,private url:UrlbaseService) {
    this._apiUrl = this.url.getapiUrl()+"/api/Entites";
  }

  /* get All Entites */
  public getAll() {
    return new Promise(resolve => {
      this.http.get(this._apiUrl ,{headers}).subscribe(data => {
        resolve(data);
      }, err => {
        console.log(err);
      });
    });
  }

  /* get Entites */
  public  getEntites(val:number): Observable<GenericEntites> {
    return this.http.get<GenericEntites>(this._apiUrl+"/"+val);
  }

  /* Add Entites */
  public setEntites(Entites:GenericEntites) {
    return new Promise(resolve => {
      this.http.post(this._apiUrl,Entites ,{headers}).subscribe(data => {
        resolve(data);
        console.log(data);
      }, err => {
        console.log(err);
      });
    });
  }

  /* Edit Entites */
  public editEntites(Entites:GenericEntites) {
    return new Promise(resolve => {
      this.http.post(this._apiUrl,Entites ,{headers}).subscribe(data => {
        resolve(data);
      }, err => {
        console.log(err);
      });
    });
  }


  /* Remove Entites */
  public removeEntites(idEntites) {
    return new Promise(resolve => {
      this.http.delete(this._apiUrl+'/'+idEntites,{headers}).subscribe(data => {
        resolve(data);
      }, err => {
        console.log(err);
      });
    });
  }





}
