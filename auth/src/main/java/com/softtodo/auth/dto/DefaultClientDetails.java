package com.softtodo.auth.dto;

import com.softtodo.auth.entities.Client;
import com.softtodo.auth.entities.Resource;
import com.softtodo.auth.entities.Role;
import com.softtodo.auth.entities.Scope;
import com.softtodo.auth.entities.User;
import com.softtodo.core.utils.SecurityUtils;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.oauth2.provider.ClientDetails;
import org.springframework.security.oauth2.provider.approval.Approval;

import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

public class DefaultClientDetails implements ClientDetails {
    private Client client;
    private Set<String> resourceIds;
    private Set<String> scopes;

    public DefaultClientDetails(Client client, Set<String> resourceIds, Set<String> scopes) {
        this.client = client;
        this.resourceIds = resourceIds == null
                ? client.getResources().stream().map(Resource::getResourceId).collect(Collectors.toSet())
                : resourceIds;
        this.scopes = scopes == null
                ? client.getScopes().stream().map(Scope::getName).collect(Collectors.toSet())
                : scopes;
    }

    @Override
    public String getClientId() {
        return client.getClientId();
    }

    @Override
    public Set<String> getResourceIds() {
        return resourceIds;
    }

    @Override
    public boolean isSecretRequired() {
        return client.isSecretRequired();
    }

    @Override
    public String getClientSecret() {
        return client.getSecret();
    }

    @Override
    public boolean isScoped() {
        return client.isScoped();
    }

    @Override
    public Set<String> getScope() {
        return this.scopes;
    }

    @Override
    public Set<String> getAuthorizedGrantTypes() {
        Set<String> result = new HashSet<>();
        result.add("refresh_token");
        switch (client.getType()){
            case SERVER:
                result.add("password");
                return result;
            case USER_GUEST:
                result.add("authorization_code");
                return result;
            case CLIENT_GUEST:
                result.add("client_credentials");
                return result;
        }
        return result;
    }

    @Override
    public Set<String> getRegisteredRedirectUri() {
        return Collections.singleton(client.getRedirectUri());
    }

    @Override
    public Collection<GrantedAuthority> getAuthorities() {
        return getGrantedAuthorities(client.getRoles());
    }

    @Override
    public Integer getAccessTokenValiditySeconds() {
        return Integer.MAX_VALUE;
    }

    @Override
    public Integer getRefreshTokenValiditySeconds() {
        return client.getTokenValidity() == null ? Integer.MAX_VALUE
                :client.getTokenValidity();
    }

    @Override
    public boolean isAutoApprove(String scope) {
        return client.getApproves().stream().anyMatch(approve -> approve.getExpiresAt().after(new Date())
                && Objects.equals(approve.getStatus(), Approval.ApprovalStatus.APPROVED)
                && Objects.equals(approve.getUser().getUsername(), SecurityUtils.currentUsername())
                && Objects.equals(approve.getScope(), scope));
    }

    @Override
    public Map<String, Object> getAdditionalInformation() {
        return new HashMap<>();
    }

    static Collection<GrantedAuthority> getGrantedAuthorities(Collection<Role> roles) {
        return roles.stream()
                .map(role -> {
                    Set<GrantedAuthority> res = new HashSet<>();
                    res.add(new SimpleGrantedAuthority(String.format("ROLE_%s",role.getName())));
                    role.getPrivileges().forEach(privilege -> {
                        res.add(new SimpleGrantedAuthority(privilege.getName()));
                    });
                    return res;
                }).reduce((res, current) -> {
                    res.addAll(current);
                    return res;
                }).orElseGet(Collections::emptySet);
    }
}
