package com.softtodo.auth.controller;

import com.softtodo.auth.entities.Approve;
import com.softtodo.core.controllers.MongoGenericController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("approve")
public class ApproveController extends MongoGenericController<Approve> {

}
