import {ChangeDetectorRef, Component, EventEmitter, Output} from '@angular/core';
import {AuthenticationService} from "../../../core/services/authentication.service";
import {TranslateService} from "@ngx-translate/core";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',

  styleUrls: ['./header.component.css']
})
export class AppHeaderComponent {

  language:string;
  @Output() notify: EventEmitter<string> = new EventEmitter<string>();

  constructor (private translate: TranslateService,public auth: AuthenticationService) {
    if(localStorage.getItem("i18n.languages")!=null)
    this.language=localStorage.getItem("i18n.languages");
    else
      this.language='en';

    translate.setDefaultLang(this.language);
  }


  logoutUser()
  {
    this.auth.logout();
  }

  showSwitchAccount()
  {
    this.auth.showSwitchAccount();
  }

  changelanguage(languageswitch:string)
  {
    this.language=languageswitch;
    this.translate.setDefaultLang(languageswitch);
    localStorage.setItem("i18n.languages",languageswitch);
    this.notify.emit(languageswitch);

  }

  folders = [
    {
      name: 'Photos',
      updated: new Date('1/1/16'),
    },
    {
      name: 'Recipes',
      updated: new Date('1/17/16'),
    },
    {
      name: 'Work',
      updated: new Date('1/28/16'),
    }
  ];
  notes = [
    {
      name: 'Vacation Itinerary',
      updated: new Date('2/20/16'),
    },
    {
      name: 'Kitchen Remodel',
      updated: new Date('1/18/16'),
    }
  ];


}
