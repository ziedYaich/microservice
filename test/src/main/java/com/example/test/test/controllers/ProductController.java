package com.example.test.test.controllers;

import com.example.test.GenericController;
import com.example.test.GenericService;
import com.example.test.test.entities.Client;
import com.example.test.test.entities.Product;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("product")
public class ProductController extends GenericController<Product> {
    
}
