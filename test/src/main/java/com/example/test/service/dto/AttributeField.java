package com.example.test.service.dto;

import com.example.test.annotations.CascadeRemove;
import com.example.test.annotations.ContentData;
import com.example.test.annotations.IgnoredAttribute;
import com.example.test.service.StructureFactory;
import org.springframework.data.domain.Example;

import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.metamodel.Attribute;
import javax.persistence.metamodel.PluralAttribute;
import javax.persistence.metamodel.SingularAttribute;
import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class AttributeField {
    private Attribute attribute ;
    private String name;
    private String type;
    private String editable = "permitAll()";
    private String condition = "true";
    private Map<String,Object> validation;
    private Field field;
    private Structure structure;
    private boolean contentData;
    private boolean cascadeRemove;
    private boolean ignored;
    private boolean manyToOne;
    private boolean oneToOne;
    private boolean manyToMany;
    private boolean oneToMany;
    private String mappedBy;


    @SuppressWarnings("unchecked")
    public AttributeField(StructureFactory structureFactory, Attribute attribute)
    {
        this.attribute=attribute;
        this.name = attribute.getName();
        this.type = attribute.getJavaType().getSimpleName();
        resolveFieldType();
        resolveFieldEditable();
        resolveFieldCondition();
        validation = new HashMap<>();
        try {

            this.field = findField(name);
            this.contentData = field.isAnnotationPresent(ContentData.class);
            this.cascadeRemove = field.isAnnotationPresent(CascadeRemove.class);
            this.ignored = field.isAnnotationPresent(IgnoredAttribute.class);
            this.mappedBy = "";
            this.manyToMany = field.isAnnotationPresent(ManyToMany.class);
            this.oneToOne = field.isAnnotationPresent(OneToOne.class);
            this.oneToMany = field.isAnnotationPresent(OneToMany.class);
            this.manyToOne = field.isAnnotationPresent(ManyToOne.class);
            if(manyToMany){
                mappedBy = field.getAnnotation(ManyToMany.class).mappedBy();
            }
            if(oneToMany){
                mappedBy = field.getAnnotation(OneToMany.class).mappedBy();
            }
        } catch (NoSuchFieldException e) {
            throw new IllegalArgumentException(attribute.getDeclaringType().getJavaType().getSimpleName()+" Invalid field "+ this.name+" "+attribute.getJavaType().getSimpleName());
        }
        resolveValidation();
        this.structure = structureFactory.getStructureForClass(attribute.getJavaType());
        if( attribute.isCollection() ){
            this.structure = structureFactory.getStructureForClass(((PluralAttribute)attribute).getElementType().getJavaType());
        }
    }

    public boolean isManyToMany() {
        return manyToMany;
    }

    public boolean isManyToOne() {
        return manyToOne;
    }

    public boolean isOneToMany() {
        return oneToMany;
    }

    public boolean isOneToOne() {
        return oneToOne;
    }

    public String getMappedBy() {
        return mappedBy;
    }

    public boolean isIgnored() {
        return ignored;
    }

    public boolean isCascadeRemove() {
        return cascadeRemove;
    }

    public boolean isContentData() {
        return contentData;
    }

    public Field getField() {
        return field;
    }

    public Structure getStructure() {
        return structure;
    }

    public Attribute getAttribute() {
        return attribute;
    }


    public void setAttribute(Attribute attribute) {
        this.attribute = attribute;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getEditable() {
        return editable;
    }

    public void setEditable(String editable) {
        this.editable = editable;
    }

    public String getCondition() {
        return condition;
    }

    public void setCondition(String condition) {
        this.condition = condition;
    }

    public Map<String, Object> getValidation() {
        return validation;
    }

    public void setValidation(Map<String, Object> validation) {
        this.validation = validation;
    }

    private void resolveValidation() {

    }

    private void resolveFieldCondition() {

    }

    private void resolveFieldEditable() {

    }

    private void resolveFieldType() {

    }
    private Field findField(String name) throws NoSuchFieldException {
        Class cl = attribute.getDeclaringType().getJavaType();
        while (!Objects.equals(cl, Object.class)){
            try{
                return cl.getDeclaredField(name);
            }catch (NoSuchFieldException e){
                cl = cl.getSuperclass();
            }
        }
        throw new NoSuchFieldException(name);
    }

}
