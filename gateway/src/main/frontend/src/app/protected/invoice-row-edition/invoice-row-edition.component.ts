import {Component, OnInit, Input, Output, EventEmitter, Injectable} from '@angular/core';


import {TableDataSource, ValidatorService} from 'angular4-material-table';

import {bounceIn, bounceOut, zoomIn} from 'ngx-animate';
import {transition, trigger, useAnimation} from "@angular/animations";
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {PersonService} from "./PersonValidator.service";
import {Observable} from "rxjs/Observable";

import {startWith} from 'rxjs/operators/startWith';
import {map} from 'rxjs/operators/map';


@Component({
  selector: 'app-invoice-row-edition',
  templateUrl: './invoice-row-edition.component.html',
  styleUrls: ['./invoice-row-edition.component.css'],
  animations: [
    trigger('triggerfadeIn', [
      transition('* => *', useAnimation(bounceIn, {params: {timing: 0.1}}))
    ]),
    trigger('zoomIn', [
      transition('* => *', useAnimation(zoomIn, {params: {timing: 0.2}}))
    ])
  ]

})
export class InvoiceRowEdition implements OnInit {


  displayedColumns = ['firstname', 'lastname', 'email', 'age', 'actionsColumn'];

  @Input() personList = [
    {firstname: 'yessine', lastname: 'abid', email: 'yessine.abid@live.fr', age: '20'},
    {firstname: 'Khaled', lastname: 'baklouti', email: 'Khaled.baklouti@live.fr', age: '20'},
    {firstname: 'ghada', lastname: 'ayadi', email: 'Khaled.baklouti@live.fr', age: '20'},
  ];
  @Output() personListChange = new EventEmitter<Person[]>();

  dataSource: TableDataSource<Person>;


  constructor() {
  }

  myControl: FormControl = new FormControl();

  ngOnInit() {
    this.dataSource = new TableDataSource<any>(this.personList);
    this.dataSource.datasourceSubject.subscribe(personList => this.personListChange.emit(personList));


  }


  options = ['yessine', 'Khaled', 'ghada'];

  filteredOptions: Observable<string[]>;


  filter(val: string) {
    if (!val) {
      this.focusAutoComplete();
      return;
    }
    console.log(val);
    let alltab = this.options.filter(option => option.toLowerCase().indexOf(val.toLowerCase()) === 0);

    console.log(alltab)
    this.filteredOptions = this.myControl.valueChanges
      .pipe(
        startWith(''),
        map(val => alltab)
      );
  }

  focusAutoComplete() {
    this.filteredOptions = this.myControl.valueChanges
      .pipe(
        startWith(''),
        map(val => [])
      );
  }


}

class Person {
  firstname: string;
  email: string;
  lastname: string;
  age: number;
}



