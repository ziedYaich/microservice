package com.example.test.test.services;

import com.example.test.GenericService;
import com.example.test.test.entities.Client;
import org.springframework.stereotype.Service;

@Service
public class ClientService extends GenericService<Client>{

}
