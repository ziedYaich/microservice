package com.example.test.service;

import com.example.test.service.dto.Structure;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import java.util.HashMap;
import java.util.Map;

@Component
public class StructureFactory {

    @Autowired
    private EntityManager em;

    private Map<Class,Structure> structureMap = new HashMap<>();

    public <T> Structure<T> getStructureForClass(Class<T> domain){
        return structureMap.get(domain);
    }

    @PostConstruct
    private void construct(){
        em.getMetamodel().getManagedTypes().forEach(managedType -> {
            structureMap.put(managedType.getJavaType(),new Structure(this,managedType));
        });
    }
}
