package com.example.test.annotations;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * Specifies that the field cannot be modified as an association in the GenericService
 * it will be ignored if the field is not an association
 * @since Java Persistence 1.0
 */
@Documented
@Target(FIELD)
@Retention(RUNTIME)
public @interface IgnoredAttribute {

}
