package com.softtodo.auth.utils;

import com.softtodo.auth.entities.Role;
import com.softtodo.auth.entities.User;
import com.softtodo.auth.repository.RoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class UserFactory {

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    private User createUser(String username, String password, String role) {
        User user = new User();
        user.setUsername(username);
        user.setPassword(passwordEncoder.encode(password));
        Role newRole = roleRepository.findByName(role);
        if (newRole == null) {
            newRole = new Role();
            newRole.setName(role);
            newRole = roleRepository.save(newRole);
        }
        user.getRoles().add(newRole);
        return user;
    }

    public User admin(String name, String description) {
        return createUser(name, description, "ROLE_ADMIN");
    }

    public User user(String name, String description) {
        return createUser(name, description, "ROLE_USER");
    }
}
