import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule} from "@angular/router";
import {FlexLayoutModule} from "@angular/flex-layout";
import {DemoMaterialModule} from "../../demo-material-module";

import {FormsModule, FormGroup, ReactiveFormsModule} from '@angular/forms';

import {  NewInvoiceRoutes} from "./invoice-row-edition.routing";

import {TranslateModule} from "@ngx-translate/core";
import {InvoiceRowEdition} from "./invoice-row-edition.component";
import {MatTableModule} from "@angular/material";
import {PersonService} from "./PersonValidator.service";
import {ValidatorService} from "angular4-material-table";



@NgModule({
  imports: [
    CommonModule,
    DemoMaterialModule,
    FlexLayoutModule,
    RouterModule.forChild(NewInvoiceRoutes),
    FormsModule,
    ReactiveFormsModule,
    TranslateModule,
    MatTableModule
  ],
  entryComponents: [
  ],
  providers: [
    {provide: ValidatorService, useClass: PersonService }
  ],
  declarations: [
    InvoiceRowEdition
  ]
})
export class NewInvoiceModule { }
