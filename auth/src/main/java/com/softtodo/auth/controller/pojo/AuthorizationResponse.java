package com.softtodo.auth.controller.pojo;

public class AuthorizationResponse
{
    private String redirectTo;

    public AuthorizationResponse(String redirectTo) {
        this.redirectTo = redirectTo;
    }

    public String getRedirectTo() {
        return redirectTo;
    }
}
