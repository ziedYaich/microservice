package com.softtodo.auth.config.security.providers;

import com.softtodo.auth.dto.DefaultUserDetails;
import com.softtodo.auth.entities.User;
import com.softtodo.auth.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

@Component
public class UserProvider implements UserDetailsService {
    @Autowired
	private UserRepository userRepository;

    @Autowired
    PasswordEncoder passwordEncoder;
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userRepository.findByUsername(username);
        if (user == null) {
            throw new UsernameNotFoundException(username);
        }
        return new DefaultUserDetails(user);
    }
}