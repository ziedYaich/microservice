import {
  AfterViewInit, Component, EventEmitter, Inject, Input, OnChanges, Output, SimpleChanges,
  ViewChild
} from '@angular/core';
import {transition, trigger, useAnimation} from "@angular/animations";
import {zoomIn} from "ngx-animate/lib";



@Component({
  selector: 'generic-datagrid',
  templateUrl: 'generic-datagrid.html',
  styleUrls: ['data-grid.css'],
  animations: [
    trigger('gridzoomIn', [
      transition(
        '* => *',
        useAnimation(zoomIn,
          {params : {timing: 0.5}}))
    ])
  ]
})
export class GenericDataGrid implements OnChanges{

    selector: string = '.main-panel';
    nbitems:number=20;

    /* Output For Action (Edit / Remove) */
    @Output() notifyAction: EventEmitter<[any,string]> = new EventEmitter<[any,string]>();

    /* Output For set data grid */
    @Input() Directive_DATA;

    user:any;
    allitem:any;

    constructor(){}

    ngOnInit(): void {
      this.allitem=this.Directive_DATA;
    }

    /* Notify Action Item */
    ActionNotify(item:any,action:string){
      this.notifyAction.emit([item,action]);
    }

    ngOnChanges(changes: SimpleChanges): void {
        this.allitem=this.Directive_DATA;
    }


    onScroll () {
      console.log('scrolled!!')
      this.notifyAction.emit([this.nbitems,"ScrollItem"]);
    }


}
