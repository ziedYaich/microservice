import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule} from "@angular/router";
import {FlexLayoutModule} from "@angular/flex-layout";
import {DemoMaterialModule} from "../../demo-material-module";

import {FormsModule, FormGroup, ReactiveFormsModule} from '@angular/forms';

import {InvoiceRoutes} from "./invoice.routing";
import {InvoiceComponent} from "./invoice.component";
import {TranslateModule} from "@ngx-translate/core";
/* Generic Components */


@NgModule({
  imports: [
    CommonModule,
    DemoMaterialModule,
    FlexLayoutModule,
    RouterModule.forChild(InvoiceRoutes),
    FormsModule,
    ReactiveFormsModule,
    TranslateModule
  ],
  entryComponents: [
  ],
  declarations: [
    InvoiceComponent
  ]
})
export class InvoiceModule { }
