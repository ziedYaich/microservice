package com.softtodo.auth.utils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.softtodo.auth.repository.ClientRepository;
import com.softtodo.auth.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;


@Component
public class Initializer implements ApplicationListener<ContextRefreshedEvent> {
    @Autowired
    ClientRepository clientRepository;
    @Autowired
    ClientFactory clientFactory;
    @Autowired
    UserFactory userFactory;
    @Autowired
    UserRepository userRepository;
    @Autowired
    ObjectMapper objectMapper;
    @Override
    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {
        if (clientRepository.count() == 0) {
            clientRepository.save(clientFactory.server("Server", "The main server"));
            clientRepository.save(clientFactory.userGuest("Guest", "Guest using users auth", "http://test","guest","guest"));
            clientRepository.save(clientFactory.clientGuest("Client admin", "Guest using its auth (admin)", "ROLE_ADMIN","guest_admin","guest"));
            clientRepository.save(clientFactory.clientGuest("Client user", "Guest using its auth (user)", "ROLE_USER","guest_user","guest"));
        }
        if (userRepository.count() == 0) {
            userRepository.save(userFactory.admin("admin", "admin"));
            userRepository.save(userFactory.user("user", "user"));
        }
    }
}
