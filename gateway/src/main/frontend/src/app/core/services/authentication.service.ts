import {Injectable} from '@angular/core';

import {Observable} from 'rxjs';
import 'rxjs/add/operator/map'
import {HttpClient, HttpHeaders, HttpParams, HttpUrlEncodingCodec} from "@angular/common/http";
import {Router} from "@angular/router";
import {isNgTemplate} from "@angular/compiler";

@Injectable()
export class AuthenticationService {
  public token: string;

  constructor(private http: HttpClient, private router: Router) {
    let currentUser = JSON.parse(localStorage.getItem('currentUser'));
    this.token = currentUser && currentUser.token;
  }

  login(username: string, password: string): Promise<any> {
    let body = new HttpParams().set('grant_type', 'password')
      .set('username', username)
      .set('password', password);
    let header = new HttpHeaders()
      .set('Authorization', 'Basic c2VydmVyOnNlcnZlcg==')
      .set('Content-Type', 'application/x-www-form-urlencoded');

    /*.then(response => {
      }).catch(error => {
        console.log(error);
      });*/
    console.log(body.toString())
    return this.http.post<any>('/uaa/oauth/token', body.toString()
      , {
        headers: header
      }
    ).map(response => {

      console.log(response)
      let users = JSON.parse(localStorage.getItem("login.current.tokens")) || [];
      let found = false;
      let user = {
        email: username,
        access_token: response.access_token,
        refresh_token: response.refresh_token,
      };
      users.forEach(value => {
        if (value.email == user.email) {
          value.access_token = user.access_token;
          value.refresh_token = user.refresh_token;
          user = value;
          found = true;
        }
      });
      if (!found) {
        users.push(user);
      }
      sessionStorage.setItem("login.current.token", JSON.stringify(user));

      localStorage.setItem("login.current.tokens", JSON.stringify(users));
      return this.checkToken(user.access_token).toPromise();
    }).toPromise();
  }

  checkToken(token): Observable<any> {
    let header = new HttpHeaders()
      .set('Authorization', 'Bearer ' + token);
    return this.http.get('/uaa/users/me'
      , {
        headers: header
      }
    );
  }

  existsMail(email: string): Promise<any> {
    let body = new HttpParams().set('email', email);
    return this.http.get('/uaa/users/exists?' + body.toString()).toPromise();
  }

  getAuthentication() {
    let user = sessionStorage.getItem("login.current.token");
    if (user) {
      return user;
    }
    let users = JSON.parse(localStorage.getItem("login.current.tokens"));
    if (users && users[0]) {
      sessionStorage.setItem("login.current.token", JSON.stringify(users[0]));
      return users[0];
    }
    return false;
  }

  logout(): void {
    this.token = null;
    localStorage.removeItem("UrlRedirect");

    localStorage.removeItem('currentUser');
    sessionStorage.removeItem('login.currentEmail');
    sessionStorage.removeItem('UrlRedirect');

    let userstokens = JSON.parse(localStorage.getItem("login.current.tokens"));
    let userstoken=JSON.parse(sessionStorage.getItem('login.current.token'));
    userstokens=userstokens.filter(item => {
      return  item.email != userstoken.email;
    } );

    localStorage.setItem("login.current.tokens",JSON.stringify(userstokens));
    sessionStorage.removeItem('login.current.token');
     this.router.navigate(['/login']);
  }


  showSwitchAccount(): void {
    localStorage.setItem("login.switch",JSON.stringify(true));
    window.open("/",'_blank');
  }

  reloadAccount(user): void {
    let users = JSON.parse(localStorage.getItem("login.current.tokens"));
    users = users.filter(element => element.email == user.email);
    if(users && users.length){
      sessionStorage.setItem("login.current.token",JSON.stringify(users[0]));
      sessionStorage.removeItem("login.switch");
      localStorage.removeItem("login.switch");
      this.router.navigate(['/']);
    }
  }

  getEmailcurrent():string
  {
    return JSON.parse(sessionStorage.getItem("login.current.token")).email;
  }

}
