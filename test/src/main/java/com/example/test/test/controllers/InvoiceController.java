package com.example.test.test.controllers;

import com.example.test.GenericController;
import com.example.test.GenericService;
import com.example.test.test.entities.Client;
import com.example.test.test.entities.Invoice;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("invoice")
public class InvoiceController extends GenericController<Invoice> {
    
}
