import {Routes} from '@angular/router';

import {FullComponent} from './layouts/full/full.component';
import {LayoutloginComponent} from "./layouts/layoutlogin/layoutlogin.component";
import {ErrorComponent} from "./public/error/error.component";
import {AuthGuard} from "./shared/guards";


export const AppRoutes: Routes = [

  {
    path: 'login',
    component: LayoutloginComponent,
    children: [
      {
        path: '',
        loadChildren: './public/login/login.module#LoginModule'
      }
    ]

  },
  {
    path: '',
    component: FullComponent,
    children: [   {
      path: '',
      loadChildren: './protected/protected.module#ProtectedComponentsModule'
    }
    ],
    canActivate: [AuthGuard]

  },

  {
    path: 'error/:typeerreur',
    component: ErrorComponent

  }



  // ,
  // { path: '**', redirectTo: 'login' }

  // ,canActivate: [AuthServiceService]
  // ,canDeactivate:[false]
];

