package com.softtodo.auth.service;

import com.softtodo.auth.entities.Client;
import com.softtodo.core.services.MongoGenericService;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ClientService extends MongoGenericService<Client> {
    public Client findByClientId(String clientId) {
        List<Client> clients = findAll(new Query().addCriteria(Criteria.where("clientId").is(clientId)));
        return clients.stream().findFirst().orElseThrow(() -> new SecurityException(""));
    }
}
