package com.softtodo.auth.controller;

import com.softtodo.auth.entities.User;
import com.softtodo.auth.service.UserService;
import com.softtodo.core.controllers.MongoGenericController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("user")
public class UserController extends MongoGenericController<User> {

}
