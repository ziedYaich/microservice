package com.example.test;

import com.example.test.search.Criteria;
import com.example.test.service.GenericServiceHolder;
import com.example.test.service.StructureFactory;
import com.example.test.service.data.support.MetamodelFullEntityInformation;
import com.example.test.service.dto.AttributeField;
import com.example.test.service.dto.Structure;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.provider.PersistenceProvider;
import org.springframework.data.jpa.repository.query.QueryUtils;
import org.springframework.data.repository.support.PageableExecutionUtils;
import org.springframework.data.util.ClassTypeInformation;
import org.springframework.data.util.TypeInformation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.metamodel.PluralAttribute;
import javax.persistence.metamodel.SingularAttribute;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Deque;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.Set;
import java.util.stream.Collectors;

import static org.springframework.data.jpa.repository.query.QueryUtils.COUNT_QUERY_STRING;
import static org.springframework.data.jpa.repository.query.QueryUtils.DELETE_ALL_QUERY_STRING;
import static org.springframework.data.jpa.repository.query.QueryUtils.applyAndBind;
import static org.springframework.data.jpa.repository.query.QueryUtils.getQueryString;

public abstract class GenericService<T extends BaseEntity> {

    @Autowired
    private EntityManager em;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private StructureFactory structureFactory;

    private Class<T> entityClass;
    private MetamodelFullEntityInformation<T, Long> jpaEntityInformation;
    private Structure<T> structure;
    private PersistenceProvider provider;
    private static final String ID_MUST_NOT_BE_NULL = "The given id must not be null!";
    private static final String ADD_COLLECTION_PREFIX = "addCollection";

    @PostConstruct
    @SuppressWarnings("unchecked")
    private void construct() {
        this.entityClass = resolveEntityClass();
        this.jpaEntityInformation = new MetamodelFullEntityInformation(this.entityClass, em.getMetamodel());
        this.structure = structureFactory.getStructureForClass(this.entityClass);
        this.provider = PersistenceProvider.fromEntityManager(em);
        GenericServiceHolder.subscribe(entityClass, this);
    }

    public Structure<T> getStructure() {
        return structure;
    }

    /**
     * Deletes a given entity.
     *
     * @param entity the entity wish will be deleted
     * @throws IllegalArgumentException in case the given entity is {@literal null}.
     */
    @Transactional
    public void delete(T entity) {
        Assert.notNull(entity, "The entity must not be null!");
        em.remove(em.contains(entity) ? entity : em.merge(entity));
    }


    /**
     * Deletes the given entities.
     *
     * @param entities the entities which will be deleted
     * @throws IllegalArgumentException in case the given {@link Iterable} is {@literal null}.
     */
    @Transactional
    public void delete(Iterable<T> entities) {

        Assert.notNull(entities, "The given Iterable of entities not be null!");

        if (!entities.iterator().hasNext()) {
            return;
        }

        applyAndBind(getQueryString(DELETE_ALL_QUERY_STRING, jpaEntityInformation.getEntityName()), entities, em)
                .executeUpdate();
    }


    /**
     * Deletes all entities managed by the service.
     */
    @Transactional
    public void deleteAll() {

        em.createQuery(getDeleteAllQueryString()).executeUpdate();
    }


    /**
     * Retrieves an entity by its id.
     *
     * @param id must not be {@literal null}.
     * @return the entity with the given id or {@literal null} if none found
     * @throws IllegalArgumentException if {@code id} is {@literal null}
     */
    public T findOne(Long id) {
        Assert.notNull(id, ID_MUST_NOT_BE_NULL);
        return em.find(entityClass, id);
    }

    /**
     * Returns whether an entity with the given id exists.
     *
     * @param id must not be {@literal null}.
     * @return true if an entity with the given id exists, {@literal false} otherwise
     * @throws IllegalArgumentException if {@code id} is {@literal null}
     */
    public boolean exists(Long id) {
        Assert.notNull(id, ID_MUST_NOT_BE_NULL);

        if (jpaEntityInformation.getIdAttribute() == null) {
            return findOne(id) != null;
        }

        String placeholder = provider.getCountQueryPlaceholder();
        String entityName = jpaEntityInformation.getEntityName();
        Iterable<String> idAttributeNames = jpaEntityInformation.getIdAttributeNames();
        String existsQuery = QueryUtils.getExistsQueryString(entityName, placeholder, idAttributeNames);

        TypedQuery<Long> query = em.createQuery(existsQuery, Long.class);

        query.setParameter(idAttributeNames.iterator().next(), id);
        return query.getSingleResult() == 1L;
    }

    /**
     * Returns all instances of the type.
     *
     * @return all entities
     */
    public List<T> findAll() {
        return getQuery(null, (Sort) null).getResultList();
    }

    /**
     * Returns all entities sorted by the given options.
     *
     * @param sort the sort options
     * @return all entities sorted by the given options
     */
    public List<T> findAll(Sort sort) {
        return getQuery(null, sort).getResultList();
    }

    /**
     * Returns a {@link Page} of entities meeting the paging restriction provided in the {@code Pageable} object.
     *
     * @param pageable paging options
     * @return a page of entities
     */
    public Page<T> findAll(Pageable pageable) {
        if (null == pageable) {
            return new PageImpl<>(findAll());
        }
        return findAll(null, pageable);
    }

    /**
     * Returns a single entity matching the given {@link Criteria} or {@literal null} if none was found.
     *
     * @param criteria can be {@literal null}.
     * @return a single entity matching the given {@link Criteria} or {@literal null} if none was found.
     * @throws org.springframework.dao.IncorrectResultSizeDataAccessException if the Criteria yields more than one result.
     */
    public T findOne(Criteria criteria) {
        try {
            return getQuery(criteria, (Sort) null).getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }

    /**
     * Returns all entities matching the given {@link Criteria}. In case no match could be found an empty {@link Iterable}
     * is returned.
     *
     * @param criteria can be {@literal null}.
     * @return all entities matching the given {@link Criteria}.
     */
    public List<T> findAll(Criteria criteria) {
        return getQuery(criteria, (Sort) null).getResultList();
    }

    /**
     * Returns a {@link Page} of entities matching the given {@link Criteria}. In case no match could be found, an empty
     * {@link Page} is returned.
     *
     * @param criteria can be {@literal null}.
     * @param pageable can be {@literal null}.
     * @return a {@link Page} of entities matching the given {@link Criteria}.
     */
    public Page<T> findAll(Criteria criteria, Pageable pageable) {
        TypedQuery<T> query = getQuery(criteria, pageable);
        return pageable == null ? new PageImpl<>(query.getResultList())
                : readPage(query, this.entityClass, pageable, criteria);
    }

    /**
     * Returns all entities matching the given {@link Criteria} applying the given {@link Sort}. In case no match could be
     * found an empty {@link Iterable} is returned.
     *
     * @param criteria can be {@literal null}.
     * @param sort     the {@link Sort} specification to sort the results by, must not be {@literal null}.
     * @return all entities matching the given {@link Criteria}.
     * @since 1.10
     */
    public List<T> findAll(Criteria criteria, Sort sort) {
        return getQuery(criteria, sort).getResultList();
    }

    /**
     * Returns the number of entities available.
     *
     * @return the number of entities
     */
    public long count() {
        return em.createQuery(getCountQueryString(), Long.class).getSingleResult();
    }

    /**
     * Returns the number of instances matching the given {@link Criteria}.
     *
     * @param criteria the {@link Criteria} to count instances for, can be {@literal null}.
     * @return the number of instances matching the {@link Criteria}.
     */
    public long count(Criteria criteria) {
        return executeCountQuery(getCountQuery(criteria, this.entityClass));
    }

    /**
     * Saves a given entity. Use the returned instance for further operations as the save operation might have changed the
     * entity instance completely.
     *
     * @param entity the entity which will be saved
     * @return the saved entity
     */
    @Transactional
    public T save(T entity) {
        if (jpaEntityInformation.isNew(entity)) {
            em.persist(entity);
            return this.findOne(entity.getId());
        } else {
            return em.merge(entity);
        }
    }

    /**
     * Saves a given JSON of an entity. Use the returned instance for further operations as the save operation might have changed the
     * entity instance completely.
     *
     * @param node the entity which will be saved
     * @return the saved entity
     */
    @Transactional
    public T save(String node) {
        try {
            JsonNode jsonNode = objectMapper.readTree(node);
            T changesEntity = objectMapper.readValue(node, this.structure.getManagedType().getJavaType());
            return save(jsonNode, changesEntity);
        } catch (IOException e) {
            throw new IllegalArgumentException("Not a valid JSON");
        }
    }


    /**
     * Saves all given entities.
     *
     * @param entities the entities which will be saved
     * @return the saved entities
     * @throws IllegalArgumentException in case the given entity is {@literal null}.
     */
    @Transactional
    public List<T> save(Iterable<T> entities) {
        List<T> result = new ArrayList<>();
        if (entities == null) {
            return result;
        }
        for (T entity : entities) {
            result.add(save(entity));
        }
        em.flush();
        return result;
    }

    @Transactional
    public T update(Long id, String node) {
        try {
            JsonNode jsonNode = objectMapper.readTree(node);
            T changesEntity = objectMapper.readValue(node, this.structure.getManagedType().getJavaType());
            T oldEntity = this.findOne(id);
            updateEntity(oldEntity, changesEntity, jsonNode);
            return this.findOne(id);
        } catch (IOException e) {
            throw new IllegalArgumentException("Not a valid JSON");
        }
    }

    @SuppressWarnings("unchecked")
    private T save(JsonNode node, T changesEntity) {
        try {
            T oldEntity = (T) changesEntity.getClass().newInstance();
            em.persist(oldEntity);
            em.refresh(oldEntity);
            updateEntity(oldEntity, changesEntity, node);
            em.persist(oldEntity);
            return this.findOne(oldEntity.getId());
        } catch (IllegalAccessException | InstantiationException e) {
            throw new IllegalArgumentException("Unknown error");
        }
    }


    private Page<T> readPage(TypedQuery<T> query, final Class<T> domainClass, Pageable pageable,
                             final Criteria criteria) {

        query.setFirstResult(pageable.getOffset());
        query.setMaxResults(pageable.getPageSize());

        return PageableExecutionUtils.getPage(query.getResultList(), pageable, () -> executeCountQuery(getCountQuery(criteria, domainClass)));
    }

    private static Long executeCountQuery(TypedQuery<Long> query) {

        Assert.notNull(query, "TypedQuery must not be null!");

        List<Long> totals = query.getResultList();
        Long total = 0L;

        for (Long element : totals) {
            total += element == null ? 0 : element;
        }

        return total;
    }

    @SuppressWarnings("unchecked")
    private TypedQuery<Long> getCountQuery(Criteria criteria, Class domainClass) {

        CriteriaBuilder builder = em.getCriteriaBuilder();
        CriteriaQuery<Long> query = builder.createQuery(Long.class);

        Root<T> root = createQuery(criteria, domainClass, query);


        if (query.isDistinct()) {
            query.select(builder.countDistinct(root));
        } else {
            query.select(builder.count(root));
        }

        // Remove all Orders the Specifications might have applied
        query.orderBy(Collections.emptyList());
        TypedQuery<Long> result = em.createQuery(query);
        if (criteria != null) {
            criteria.process(result);
        }
        return result;
    }

    private String getDeleteAllQueryString() {
        return getQueryString(DELETE_ALL_QUERY_STRING, jpaEntityInformation.getEntityName());
    }

    private String getCountQueryString() {

        String countQuery = String.format(COUNT_QUERY_STRING, provider.getCountQueryPlaceholder(), "%s");
        return getQueryString(countQuery, jpaEntityInformation.getEntityName());
    }


    private TypedQuery<T> getQuery(Criteria criteria, Pageable pageable) {

        Sort sort = pageable == null ? null : pageable.getSort();
        return getQuery(criteria, this.entityClass, sort);
    }

    private TypedQuery<T> getQuery(Criteria criteria, Sort sort) {
        return getQuery(criteria, this.entityClass, sort);
    }

    private TypedQuery<T> getQuery(Criteria criteria, Class<T> domainClass, Sort sort) {

        CriteriaBuilder builder = em.getCriteriaBuilder();
        CriteriaQuery<T> query = builder.createQuery(domainClass);
        Root<T> root = createQuery(criteria, domainClass, query);
        query.select(root);
        if (sort != null) {
            query.orderBy(QueryUtils.toOrders(sort, root, builder));
        }
        TypedQuery<T> typedQuery = em.createQuery(query);
        if (criteria != null) {
            criteria.process(typedQuery);
        }

        return typedQuery;
    }

    @SuppressWarnings("unchecked")
    private Root<T> createQuery(Criteria criteria, Class<T> domainClass,
                                CriteriaQuery query) {
        Assert.notNull(domainClass, "Domain class must not be null!");
        Assert.notNull(query, "CriteriaQuery must not be null!");

        Root<T> root = query.from(domainClass);

        if (criteria == null) {
            return root;
        }

        CriteriaBuilder builder = em.getCriteriaBuilder();
        Predicate predicate = criteria.toPredicate(root, query, builder);

        if (predicate != null) {
            query.where(predicate);
        }

        return root;
    }

    @SuppressWarnings("unchecked")
    private Class<T> resolveEntityClass() {
        TypeInformation<?> information = ClassTypeInformation.from(getClass());
        List<TypeInformation<?>> arguments = information.getSuperTypeInformation(GenericService.class).getTypeArguments();
        if (!arguments.isEmpty() && arguments.get(0) != null) {
            return ((TypeInformation) arguments.get(0)).getType();
        } else {
            throw new IllegalArgumentException(String.format("Could not resolve domain type of %s!", getClass()));
        }
    }


    private void updateEntity(T oldEntity, T changesEntity, JsonNode node) {
        this.structure.getAttributes().forEach(attribute -> {
            if (attribute.getAttribute() instanceof SingularAttribute &&
                    ((SingularAttribute) attribute.getAttribute()).isId()) {
                return;
            }
            Field field = attribute.getField();
            if (node.has(field.getName())) {
                updateField(attribute, getValueOfField(field, changesEntity), oldEntity, node);
            }
        });
    }

    @SuppressWarnings("unchecked")
    private void updateField(AttributeField attributeField, Object changeValue, T oldEntity, JsonNode node) {

        switch (attributeField.getAttribute().getPersistentAttributeType()) {
            case EMBEDDED:
            case BASIC:
                updateBasicField(attributeField, changeValue, oldEntity);
                return;
            case ELEMENT_COLLECTION:
                updateElementCollectionField(attributeField, changeValue, oldEntity, node.has(ADD_COLLECTION_PREFIX + attributeField.getName()));
                return;
            case ONE_TO_ONE:
            case MANY_TO_ONE:
                updateToOneField(attributeField, changeValue, oldEntity, node.get(attributeField.getName()));
                return;
            case ONE_TO_MANY:

            case MANY_TO_MANY:
                updateToManyField(attributeField, changeValue, oldEntity, node);
        }
    }

    private void updateBasicField(AttributeField attributeField, Object changeValue, T oldEntity) {
        setValueOfField(attributeField.getField(), oldEntity, changeValue);
    }

    @SuppressWarnings("unchecked")
    private void updateElementCollectionField(AttributeField attributeField, Object changeValue, T oldEntity, boolean add) {
        if (!add) {
            setValueOfField(attributeField.getField(), oldEntity, changeValue);
            return;
        }
        if (changeValue instanceof Collection) {
            Collection collectionChangeValue = (Collection) changeValue;
            Collection collectionOldValue = (Collection) getValueOfField(attributeField.getField(), oldEntity);
            collectionOldValue.addAll(collectionChangeValue);
            return;
        }
        if (changeValue instanceof Map) {
            Map mapChangeValue = (Map) changeValue;
            Map mapOldValue = (Map) getValueOfField(attributeField.getField(), oldEntity);
            mapOldValue.putAll(mapChangeValue);
            return;
        }
        setValueOfField(attributeField.getField(), oldEntity, changeValue);
    }

    private void updateToOneField(AttributeField attributeField, Object changeValue, T oldEntity, JsonNode node) {
        if (attributeField.isIgnored()) {
            throw new IllegalArgumentException(
                    String.format("Trying to edit a not editable field %s", attributeField.getField().getName()));
        }
        if (changeValue == null) {
            setValueOfField(attributeField.getField(), oldEntity, null);
            return;
        }
        if (changeValue instanceof BaseEntity) {
            BaseEntity changeBaseEntity = (BaseEntity) changeValue;
            setValueOfField(attributeField.getField(), oldEntity, getSingleAssociation(changeBaseEntity, attributeField, node));
            return;
        }
        throw new IllegalArgumentException("Illegal ManyToOne Entity type : %s (must be extends BaseEntity)");
    }

    private void updateToManyField(AttributeField attributeField, Object changeValue, T oldEntity, JsonNode node) {
        if (attributeField.isIgnored()) {
            throw new IllegalArgumentException(
                    String.format("Trying to edit a not editable field %s", attributeField.getField().getName()));
        }
        if (changeValue == null) {
            setValueOfField(attributeField.getField(), oldEntity, null);
            return;
        }
        getMultipleAssociation(oldEntity, node.get(attributeField.getName()), attributeField, (Collection) changeValue, node.has(ADD_COLLECTION_PREFIX + attributeField.getName()));
    }

    @SuppressWarnings("unchecked")
    private BaseEntity getSingleAssociation(BaseEntity entity, AttributeField attributeField, JsonNode node) {
        return getSingleAssociation(entity, attributeField, node, attributeField.getField().getType());
    }

    @SuppressWarnings("unchecked")
    private BaseEntity getSingleAssociation(BaseEntity entity, AttributeField attributeField, JsonNode node, Class fieldType) {
        GenericService service = GenericServiceHolder.getServiceForClass((Class<? extends BaseEntity>) fieldType);
        if (service == null) {
            throw new IllegalArgumentException(String.format("Not a managed type %s", fieldType));
        }
        if (entity.getId() != null) {
            BaseEntity result = service.findOne(entity.getId());
            Assert.notNull(result, String.format("No %s with id %s", entity.getClass().getSimpleName(), entity.getId()));
            if (attributeField.isContentData()) {
                service.updateEntity(result, entity, node);
                em.merge(result);
            }
            return result;
        }
        if (attributeField.isContentData()) {
            return service.save(node.toString());
        }
        throw new IllegalArgumentException(String.format("Try to pass the id of : %s", attributeField.getField().getName()));
    }

    @SuppressWarnings("unchecked")
    private void getMultipleAssociation(T entity, JsonNode node, AttributeField attributeField, Collection list, boolean add) {
        Field field = attributeField.getField();
        if (getValueOfField(field, entity) == null) {
            setValueOfField(field, entity, initializeCollection(field));
        }
        Collection oldValue = (Collection) getValueOfField(field, entity);
        //boolean add = node.has(ADD_COLLECTION_PREFIX + field.getName());
        Map<Long, BaseEntity> oldMap = new HashMap<>();
        oldValue.forEach(element -> oldMap.put(((BaseEntity) element).getId(), (BaseEntity) element));
        oldValue.clear();
        Map<Long, BaseEntity> newMap = new HashMap<>();
        int index = 0;
        for (Object aList : list) {
            BaseEntity newEntity = getSingleAssociation(((BaseEntity) aList), attributeField, node.get(index++), ((PluralAttribute) attributeField.getAttribute()).getElementType().getJavaType());
            newMap.put(newEntity.getId(), newEntity);
        }
        oldValue.addAll(newMap.values());
        oldMap.keySet()
                .stream()
                .filter(t -> !newMap.containsKey(t))
                .collect(Collectors.toSet())
                .forEach(key -> {
                    if (add) {
                        oldValue.add(oldMap.remove(key));
                    } else {
                        detachEntity(entity, attributeField, oldMap.remove(key));
                    }
                });
        oldValue.forEach(t -> attachEntity(entity, attributeField, (BaseEntity) t));
    }

    @SuppressWarnings("unchecked")
    private void detachEntity(T entity, AttributeField attributeField, BaseEntity detach) {
        Structure structure = structureFactory.getStructureForClass(detach.getClass());
        GenericService service = GenericServiceHolder.getServiceForClass(detach.getClass());
        BaseEntity oldDetachEntity = service.findOne(detach.getId());
        if ((attributeField.isOneToMany() || (attributeField.isOneToOne()))
                && !attributeField.getMappedBy().isEmpty()) {
            setValueOfField(structure.getAttribute(attributeField.getMappedBy()).getField(),
                    oldDetachEntity, null);
            service.save(oldDetachEntity);
            return;
        }
        if (attributeField.isManyToMany() && !attributeField.getMappedBy().isEmpty()) {
            Collection collection = (Collection) getValueOfField(structure.getAttribute(attributeField.getMappedBy()).getField(),
                    oldDetachEntity);
            collection.remove(entity);
            service.save(oldDetachEntity);
        }
    }

    @SuppressWarnings("unchecked")
    private void attachEntity(T entity, AttributeField attributeField, BaseEntity detach) {
        Structure structure = structureFactory.getStructureForClass(detach.getClass());
        GenericService service = GenericServiceHolder.getServiceForClass(detach.getClass());
        BaseEntity oldDetachEntity = service.findOne(detach.getId());
        if ((attributeField.isOneToMany() || (attributeField.isOneToOne()))
                && !attributeField.getMappedBy().isEmpty()) {
            setValueOfField(structure.getAttribute(attributeField.getMappedBy()).getField(),
                    oldDetachEntity, entity);
            service.save(oldDetachEntity);
            return;
        }
        if (attributeField.isManyToMany() && !attributeField.getMappedBy().isEmpty()) {
            Collection collection = (Collection) getValueOfField(structure.getAttribute(attributeField.getMappedBy()).getField(),
                    oldDetachEntity);
            if (collection.contains(entity)) {
                collection.add(entity);
                service.save(oldDetachEntity);
            }
        }
    }

    private Collection initializeCollection(Field field) {

        Class clazz = field.getType();
        if (!Collection.class.isAssignableFrom(clazz)) {
            throw new IllegalArgumentException(String.format("trying to initialize non collection field %s", field));
        }
        @SuppressWarnings("unchecked")
        Class<? extends Collection> cl = (Class<? extends Collection>) field.getType();
        if (cl.equals(Set.class)) {
            return new HashSet<>();
        }
        if (cl.equals(List.class)) {
            return new ArrayList<>();
        }
        if (cl.equals(Queue.class)) {
            return new PriorityQueue<>();
        }
        if (cl.equals(Deque.class)) {
            return new ArrayDeque<>();
        }
        if (cl.equals(Deque.class)) {
            return new ArrayDeque<>();
        }
        if (cl.equals(Collection.class)) {
            return new ArrayList<>();
        }
        try {
            return cl.newInstance();
        } catch (InstantiationException | IllegalAccessException e) {
            throw new IllegalArgumentException(String.format("You need to initialize the collection field %s", field));
        }
    }

    public String getEntityName() {
        return structure.getName();
    }

    private void setValueOfField(Field field, Object destination, Object value) {
        try {
            boolean accessible = field.isAccessible();
            field.setAccessible(true);
            field.set(destination, value);
            field.setAccessible(accessible);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

    private Object getValueOfField(Field field, Object source) {
        try {
            boolean accessible = field.isAccessible();
            field.setAccessible(true);
            Object value = field.get(source);
            field.setAccessible(accessible);
            return value;
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }
}
