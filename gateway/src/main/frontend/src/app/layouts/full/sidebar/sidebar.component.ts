import {ChangeDetectorRef, Component} from '@angular/core';
import {MediaMatcher} from '@angular/cdk/layout';
import {MenuItems} from '../../../shared/menu-items/menu-items';
import {AuthenticationService} from "../../../core/services/authentication.service";


import { TranslateService } from '@ngx-translate/core';


@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls:['./sidebar.component.scss']
})
export class AppSidebarComponent {

    mobileQuery: MediaQueryList;
    emailcurrent=this.auth.getEmailcurrent();

    private _mobileQueryListener: () => void;

    constructor(private translate: TranslateService,public auth: AuthenticationService,changeDetectorRef: ChangeDetectorRef, media: MediaMatcher, public menuItems: MenuItems) {
      this.mobileQuery = media.matchMedia('(min-width: 768px)');
      this._mobileQueryListener = () => changeDetectorRef.detectChanges();
      this.mobileQuery.addListener(this._mobileQueryListener);
    }

    ngOnDestroy(): void {
      this.mobileQuery.removeListener(this._mobileQueryListener);
    }

}
