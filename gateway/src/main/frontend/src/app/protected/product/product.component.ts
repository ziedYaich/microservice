import {Component, Input, OnChanges, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {MatDialog} from "@angular/material";
import {ModalRemove} from "../../shared/GenericCRUD/genericDialogRemove/modalRemove.component";
import {bounceIn, zoomIn} from "ngx-animate/lib";
import {transition, trigger, useAnimation} from "@angular/animations";

/*Filter*/
import {ENTER, COMMA} from '@angular/cdk/keycodes';
import {MatChipInputEvent} from '@angular/material';


@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css'],
  animations: [
      trigger('triggerfadeIn', [
        transition( '* => *', useAnimation(bounceIn, {params : {timing: 0}}))
      ]),
      trigger('zoomIn', [
        transition( '* => *', useAnimation(zoomIn, {params : {timing: 1}}))
      ])
  ]
})
export class ProductComponent implements OnInit {

      /* Set a Structure Fields To Generic Add */
      structureFields:any = Structure_Add;
      titleComponent:any;
      /* Set a data To dataGrid */
      dataSource:any = Directive_DATA;
      /* Set a itemDetail To Generic Detail*/
      itemDetail:any;
      /* Set a Data Detail To Generic Detail*/
      dataEdit:any;

      rootview:any;

      /* Filter */
      visible: boolean = true;
      selectable: boolean = true;
      removable: boolean = true;
      addOnBlur: boolean = true;
      // Enter, comma
      separatorKeysCodes = [ENTER, COMMA];

      chipFilter =[];
      private id;

      constructor(public dialog: MatDialog,private  router : Router ,private route: ActivatedRoute) {}

      ngOnInit(): void {

        this.titleComponent=this.structureFields.name+".add";

          this.route.paramMap.subscribe(params => {
            this.rootview = params.get('rootview');
            if(this.rootview !='add' && this.rootview !='list' && this.rootview !='edit' && this.rootview !='detail')
             { this.router.navigate(['/error/404']); }
          });
      }

      /* Notify Action Product */
      NotifyAction(eventNotify:any){

          if(eventNotify[1]=='add')
          {
            this.titleComponent=this.structureFields.name+".add";
            this.rootview='add';
            this.dataEdit=[];
            this.router.navigate(['product/add']);
          }
          else if(eventNotify[1]=='edit')
          {
            this.titleComponent=this.structureFields.name+".edit";
            this.rootview='edit';
            this.dataEdit=eventNotify[0];
            this.router.navigate(['product/edit']);
          }
          else if(eventNotify[1]=='detail')
          {
            this.rootview='detail';
            this.itemDetail=eventNotify[0];
            this.router.navigate(['product/detail']);
          }
          else if(eventNotify[1]=='remove')
          {
            this.removeProduct(eventNotify[0]);
          }
          else if(eventNotify[1]=='ScrollItem')
          {
            this.AddScrollItem();
          }

      }

      /* Remove product */
      removeProduct(product:any)  {
         let item=product.text;
            let dialogRef = this.dialog.open(ModalRemove,
              { height: 'auto', width: '600px'  , data: { itemRemove: 'Product'}});

            const sub = dialogRef.componentInstance.RemoveAction.subscribe((data) => {
              console.log(data);
            });
            dialogRef.afterClosed().subscribe(result => {
              console.log('The dialog was closed');
            });
      }

      AddScrollItem(){
        for(let i=0;i<5;i++)
          Directive_DATA.push({ Nom: 'Dell', Email:'user@dell.com'}  )

      }

      applyFilter(filterValue: string) {

          filterValue = filterValue.trim();
          filterValue = filterValue.toLowerCase();

          if (filterValue != '') {
            this.dataSource = this.dataSource.filter((item) => {
              return item.Nom.toLowerCase().indexOf(filterValue) > -1 || item.Email.toLowerCase().indexOf(filterValue) > -1;
            })
          }

          if (filterValue== '' && this.chipFilter!=[]) {
              for(let i =0; i < this.chipFilter.length;i++ ) {
                this.applyFilter(this.chipFilter[i]);
              }
          }

      }

      keydown(event,filterValue: string)
      {
          const key = event.key;
          if ( key === "Backspace") {
              this.dataSource=Directive_DATA;

              for(let i =0; i < this.chipFilter.length;i++ ) {
                this.dataSource = this.dataSource.filter((item) => {
                  return item.Nom.toLowerCase().indexOf(this.chipFilter[i].toLowerCase()) > -1 || item.Email.toLowerCase().indexOf(this.chipFilter[i].toLowerCase()) > -1;
                });
              }

              this.dataSource = this.dataSource.filter((item) => {
                return item.Nom.toLowerCase().indexOf(filterValue) > -1 || item.Email.toLowerCase().indexOf(filterValue) > -1;
              });
          }

      }

      add(event: MatChipInputEvent): void {

          let input = event.input;
          let value = event.value;

          // Add our fruit
          if ((value || '').trim()) {
            this.chipFilter.push(value.trim());
            this.applyFilter(value);
          }
          // Reset the input value
          if (input) {
            input.value = '';
          }
      }

      remove(itemChip: any): void {
          let index = this.chipFilter.indexOf(itemChip);

          if (index >= 0) {
            this.chipFilter.splice(index, 1);
          }

          if(this.chipFilter)
          {
            this.dataSource=Directive_DATA;
           for(let i =0; i < this.chipFilter.length;i++ ) {
              this.applyFilter(this.chipFilter[i]);
            }
          }
      }

}


const Structure_Add:any =
    {
      "name":"product",
      "displaySingularName":"Product",
      "displayPluralName":"Products",
      "fields":[
        {
          "name":"Nom",
          "displayName":"Nom",
          "type":{
            "name":"string",
            "required":true,
            "editable":true,
            "minLength":3,
            "maxLength":20
          }
        },
        {
          "name":"price",
          "displayName":"Prix",
          "type":{
            "name":"double",
            "required":true,
            "editable":true,
            "minValue":0.01
          }
        },
        {
          "name":"Email",
          "displayName":"Email",
          "type":{
            "name":"email",
            "required":true,
            "editable":true,
            "minLength":3,
            "maxLength":20
          }
        },
        {
          "name":"commercials",
          "displayName":"Commerciales",
          "type":{
            "name":"list",
            "elementType":[
                { "id":1,  "name":"yessine", "lastname":"abid"  },
                {  "id":2,  "name":"zied",  "lastname":"yaich"  },
                { "id":3,  "name":"khaled",  "lastname":"baklouti" }  ],
            "required":false,
            "editable":true,
            "maxLength":5
          }
        },
        {
          "name":"days",
          "displayName":"Days",
          "type":{
            "name":"enumeration",
            "elementType":[
                { "id":1, "name":"Monday" },
                {  "id":2, "name":"Tuesday" },
                {  "id":3, "name":"Wednesday " }  ],
            "required":true,
            "editable":true,
            "multiple":true
          }
        },
        {
          "name":"color",
          "displayName":"Color",
          "type":{
            "name":"enumeration",
            "elementType":[
              { "id":1, "name":"red" },
              {  "id":2, "name":"bleu" }
              ],
            "required":true,
            "editable":true,
            "multiple":false
          }
        },
        {
          "name":"Description",
          "displayName":"Description",
          "type":{
            "name":"Textarea",
            "required":true,
            "editable":true,
            "minLength":3,
            "maxLength":20
          }
        }
      ]
    } ;



const Directive_DATA:any = [
  {
    Nom: 'Dell',
    Email:'user@dell.com',
    days:"Monday",
    color:"bleu",
    commercials:2,
    Description: "dell 2018",
    price:10.2


  },
  {
    Nom: 'acer',
    Email:'user@acer.com',
    days:"Tuesday",
    color:"red",
    commercials:1,
    Description: "acer 2018",
    price:20.20
  },
  {
    Nom: 'hp',
    Email:'user@hp.com',
    days:"Monday"
  },
  {
    Nom: 'asus',
    Email:'user@asus.com',
    days:"Monday"
  },
  {
    Nom: 'Dell 5521',
    Email:'user@Dell.com',
    days:"Monday"
  },
  {
    Nom: 'lenovo',
    Email:'user@lenovo.com',
    days:"Monday"
  },
  {
    Nom: 'lenovo',
    Email:'user@lenovo.com',
    days:"Monday"
  },
  {
    Nom: 'lenovo',
    Email:'user@lenovo.com',
    days:"Monday"
  },
  {
    Nom: 'lenovo',
    Email:'user@lenovo.com',
    days:"Monday"
  },
  {
    Nom: 'lenovo',
    Email:'user@lenovo.com',
    days:"Monday"
  }, {
    Nom: 'Dell',
    Email:'user@dell.com',
    days:"Monday"
  },
  {
    Nom: 'acer',
    Email:'user@acer.com',
    days:"Monday"
  },
  {
    Nom: 'hp',
    Email:'user@hp.com',
    days:"Monday"
  },
  {
    Nom: 'asus',
    Email:'user@asus.com',
    days:"Monday"
  },
  {
    Nom: 'Dell 5521',
    Email:'user@Dell.com',
    days:"Monday"
  },
  {
    Nom: 'lenovo',
    Email:'user@lenovo.com',
    days:"Monday"
  },
  {
    Nom: 'lenovo',
    Email:'user@lenovo.com',
    days:"Monday"
  },
  {
    Nom: 'lenovo',
    Email:'user@lenovo.com',
    days:"Monday"
  },
  {
    Nom: 'lenovo',
    Email:'user@lenovo.com',
    days:"Monday"
  },
  {
    Nom: 'lenovo',
    Email:'user@lenovo.com',
    days:"Monday"
  }
];
