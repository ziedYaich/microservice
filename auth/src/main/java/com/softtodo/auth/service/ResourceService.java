package com.softtodo.auth.service;

import com.softtodo.auth.entities.Resource;
import com.softtodo.core.services.MongoGenericService;
import org.springframework.stereotype.Service;

@Service
public class ResourceService extends MongoGenericService<Resource> {
}
