import { Injectable } from '@angular/core';
import {Router, CanActivate, RouterStateSnapshot, ActivatedRouteSnapshot} from '@angular/router';
import {Observable} from "rxjs/Observable";
import {HttpParams} from "@angular/common/http";

@Injectable()
export class AuthGuard implements CanActivate {

  constructor(private router: Router) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean{
    let users :any;
    let user:any;

    if(JSON.parse(localStorage.getItem("login.switch"))){
      localStorage.removeItem("login.switch");
      sessionStorage.setItem("login.switch",JSON.stringify(true));
      sessionStorage.removeItem("login.current.token");
      this.router.navigate(['/login' ] );
      return false;
    }
    let localStorageTokens=localStorage.getItem("login.current.tokens");
    let sessionStorageToken=sessionStorage.getItem("login.current.token");

    if(sessionStorageToken!=null&&localStorageTokens!=null)
    {
      users = JSON.parse(localStorageTokens);
      user = JSON.parse(sessionStorageToken);


          users = users.filter(element => user&&(element.email == user.email));
          if(!(users && users.length)){
            sessionStorage.removeItem("login.current.token");
          }

          if(!user) {
            let users = JSON.parse(localStorageTokens);

            if(!users || !users[0]){
              sessionStorage.setItem("UrlRedirect",state.url);
              this.router.navigate(['/login' ] );
              return false;
            }else {
              user = users[0];
              sessionStorage.setItem("login.current.token",JSON.stringify(user))
            }
          }

    }
    else
    {
      this.router.navigate(['/login' ] );
      return false;
    }

    return true;
  }
}
