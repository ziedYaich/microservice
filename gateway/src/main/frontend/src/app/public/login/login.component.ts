import {Component , OnInit} from '@angular/core';


import {FormControl, Validators} from '@angular/forms';

import {ActivatedRoute, Router} from '@angular/router';
import {ValidationErrors} from "@angular/forms/src/directives/validators";
import {MatAutocompleteModule, MatSnackBar} from "@angular/material";
import {Observable} from "rxjs/Observable";
import {startWith} from "rxjs/operators/startWith";
import {map} from "rxjs/operator/map";
import {AuthenticationService} from "../../core/services/authentication.service";
import {TranslateService} from "@ngx-translate/core";


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  showPassword = false;
  viewProgress= false;
  currentEmail = sessionStorage.getItem("login.currentEmail") || false;
  filteredAutoCompleteEmails;
  redirect;
  private sub: any;
  language='en';
  connectedUsers = JSON.parse(localStorage.getItem("login.current.tokens"));

  email = new FormControl('', [Validators.required, Validators.email]);
  password = new FormControl('', [Validators.required]);

  constructor(private translate: TranslateService,public snackBar: MatSnackBar,private  router : Router,private route: ActivatedRoute,private auth : AuthenticationService) {

    this.email.valueChanges.subscribe(value => {
      this.filteredAutoCompleteEmails = this.filterStates(value);
    })

    if(localStorage.getItem("i18n.languages")!=null)
      this.language=localStorage.getItem("i18n.languages");
    else
      this.language='en';

    translate.setDefaultLang(this.language);
  }
  ngOnInit() {
    if((!JSON.parse(sessionStorage.getItem("login.switch")))&&(this.connectedUsers&&this.connectedUsers.length))
    {
      this.router.navigate(['/']);
    }
  }


  verifyEmailkeypress(event){
    if(event.keyCode == 13) {
      this.verifyEmail();
    }
  }

  verifyEmail(){
    this.viewProgress=true;
    this.auth.existsMail(this.email.value).then(response => {
      if(response && response.value)
      {
        this.currentEmail=response.value;
        let validEmails = JSON.parse(localStorage.getItem("login.validEmails"));
        if(validEmails == null){
          validEmails = [];
        }
        if(validEmails.indexOf(this.currentEmail) < 0){
          validEmails.push(this.currentEmail);
          localStorage.setItem("login.validEmails",JSON.stringify(validEmails));
        }
        sessionStorage.setItem("login.currentEmail", ''+this.currentEmail);
        this.viewProgress=false;
      }
      else{
        this.viewProgress=false;
        console.log("invalid email")
        this.emailIncorrect();
      }
    }).then(value => {
      console.log("cc"+value);
      this.viewProgress=false;
    }).catch(reason => {


      setTimeout(()=>{    this.viewProgress=false;},1000);
    });;

  }

  anotherAccount(){
    this.email.reset();
    localStorage.removeItem("login.currentEmail");
    this.currentEmail = false;
  }


  connectKeypress(event){
      if(event.keyCode == 13) {
        this.connect();
      }
  }
  connect()
  {
    this.auth.login(''+this.currentEmail, this.password.value).then(user => {
      if(user){
        sessionStorage.removeItem("login.currentEmail");
        sessionStorage.removeItem("login.switch");
        this.redirect = sessionStorage.getItem("UrlRedirect");
        this.router.navigate([this.redirect || '/']);
      }
    }).catch(reason => {
      console.log(reason);
      this.passwordIncorrect();
    });
  }

  getErrorMessage() {
    return this.email.hasError('required') ? 'You must enter a value' :
      this.email.hasError('email') ? 'Not a valid email' :  '';

  }

  getErrorMessagePass(){
    return this.password.hasError('required') ? 'You must enter a value' :
      this.password.hasError('password') ? 'Not a valid password' :  '';
  }

  getsuccessMessage(){
    return '';
  }

  filterStates(email: string)
  {
      let autoCompleteEmails = JSON.parse(localStorage.getItem("login.validEmails")) || []
      if(! autoCompleteEmails){
        return [];
      }
      return autoCompleteEmails.filter(option =>
        option.toLowerCase().indexOf(email && email.toLowerCase()) === 0);
  }

  connectSwitch(user){
    this.auth.reloadAccount(user);
  }


  emailIncorrect() {
    this.snackBar.open("Email Incorrect", "Close", {
      duration: 1000, extraClasses: ['error-snackbar']
    });
  }


  passwordIncorrect() {
    this.snackBar.open("Password Incorrect", "Close", {
      duration: 1000
    }) ;

  }

  changelanguage(languageswitch:string)
  {
    this.language=languageswitch;
    this.translate.setDefaultLang(languageswitch);
    localStorage.setItem("i18n.languages",languageswitch);
  }

  removeAccount(userRemove:any){
      let userstokens = JSON.parse(localStorage.getItem("login.current.tokens"));
      userstokens=userstokens.filter(item => {
        return  item.email != userRemove.email;
      } );
      localStorage.setItem("login.current.tokens",JSON.stringify(userstokens));
      this.connectedUsers = JSON.parse(localStorage.getItem("login.current.tokens"));
  }


}

