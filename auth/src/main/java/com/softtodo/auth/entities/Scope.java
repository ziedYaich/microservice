package com.softtodo.auth.entities;

import com.softtodo.core.entities.BaseEntity;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Objects;

@Document
public class Scope extends BaseEntity{

    private String name;

    private String description;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Scope scope = (Scope) o;
        return Objects.equals(name, scope.name);
    }

    @Override
    public int hashCode() {

        return Objects.hash(name);
    }

}
