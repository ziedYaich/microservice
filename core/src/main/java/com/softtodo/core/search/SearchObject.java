package com.softtodo.core.search;

import org.springframework.data.mongodb.core.query.Criteria;

public interface SearchObject {
    Criteria toCriteria();
}
