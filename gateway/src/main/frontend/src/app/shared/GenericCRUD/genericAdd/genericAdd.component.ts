import {AfterViewInit, Component, EventEmitter, Inject, Input, OnInit, Output, ViewChild} from '@angular/core';

import {  MatPaginator, MatSort, MatTableDataSource,
  MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import {FormBuilder, FormGroup, FormControl, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from "@angular/router";
import {validate} from "codelyzer/walkerFactory/walkerFn";

@Component({
  selector: 'generic-add',
  templateUrl: 'generic-add.html',
  styleUrls: ['generic-add.css'],
})
export class GenericAdd implements AfterViewInit,OnInit{

  /* Structure Fields Add */
  @Input() Structure_Fields;
  @Input() TitleComponent;
  @Input() DataEdit;

  @Output() AddActionNotify: EventEmitter<any> = new EventEmitter<any>();

  //email = new FormControl('', [Validators.required, Validators.email]);

  form: FormGroup;
  structureFields:any;

  formvalid:any={};

  buttonStatus:any;
  rootview:any;

  constructor(private  router : Router , private fb: FormBuilder ,private route: ActivatedRoute) {}


  /* Notify Action Item */
  SaveDataNotify(form){
    this.AddActionNotify.emit(form);
  }


  ngAfterViewInit(): void {}

  ngOnInit(): void {

    this.buttonStatus ='desabled';
    this.structureFields=this.Structure_Fields;
    for(let item of  this.structureFields.fields)  {
      if(item.type.editable && item.type.required){
        if(item.type.name=='email')
        { this.formvalid[item.name]= [null, Validators.required];}
        else
        { this.formvalid[item.name]= [null, Validators.required];}
      }
    }
    this.form = this.fb.group(this.formvalid);



    if(this.DataEdit==null) {
      this.DataEdit = [];
      this.buttonStatus = 'enabled';
      console.log("ngOnInit DataEdit="+this.DataEdit)
    }


  }

  ngOnChanges(){


    console.log(" ngOnChanges buttonStatus : "+ this.buttonStatus)
    console.log(" ngOnChanges DataEdit : "+ this.DataEdit)
    if(this.DataEdit==[]) {
      this.buttonStatus = 'desabled';
    }else
    {
      this.buttonStatus = 'enabled';
    }
    console.log("------ ")
    console.log(" ngOnChanges DataEdit : "+ this.DataEdit)
    console.log(" ngOnChanges buttonStatus : "+ this.buttonStatus)
  }



}

