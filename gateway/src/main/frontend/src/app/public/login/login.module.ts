import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {CommonModule, HashLocationStrategy, LocationStrategy} from '@angular/common';
import {DemoMaterialModule} from '../../demo-material-module';
import {FlexLayoutModule} from '@angular/flex-layout';
import {LoginRoutes} from './login.routing';

import {LoginComponent} from "./login.component";
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MatAutocompleteModule} from "@angular/material";
import {AuthenticationService} from "../../core/services/authentication.service";
import {HttpClient} from "@angular/common/http";



/*  Translate */
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}
@NgModule({
  imports: [
    CommonModule,
    DemoMaterialModule,
    FlexLayoutModule,
    FormsModule,
    ReactiveFormsModule,
    MatAutocompleteModule,
    RouterModule.forChild(LoginRoutes),
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    })


  ],
  entryComponents:[ ],
  declarations: [ LoginComponent ],
  providers: [
    AuthenticationService
  ]
})

export class LoginModule {}
