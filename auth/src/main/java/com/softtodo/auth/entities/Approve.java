package com.softtodo.auth.entities;

import com.softtodo.core.entities.BaseEntity;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.security.oauth2.provider.approval.Approval;

import java.util.Calendar;
import java.util.Date;
import java.util.Objects;

@Document
public class Approve extends BaseEntity {

    @DBRef
    private User user;

    @DBRef
    private Client client;


    private String scope;

    private Approval.ApprovalStatus status;

    private Date expiresAt;

    private Date lastUpdatedAt;

    public Approve() {
    }

    private Approve(User user, Client client, String scope, Date expiresAt, Approval.ApprovalStatus status, Date lastUpdatedAt) {
        this.user = user;
        this.client = client;
        this.scope = scope;
        this.expiresAt = expiresAt;
        this.status = status;
        this.lastUpdatedAt = lastUpdatedAt;
    }


    public Approve(Approval approval, User user, Client client) {
        this(user, client,
                approval.getScope(), approval.getExpiresAt(),
                approval.getStatus(), approval.getLastUpdatedAt());
        Calendar unlimitedExpiration = Calendar.getInstance();
        unlimitedExpiration.add(Calendar.MINUTE, Integer.MAX_VALUE);
        setExpiresAt(unlimitedExpiration.getTime());
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public String getScope() {
        return scope;
    }

    public void setScope(String scope) {
        this.scope = scope;
    }

    public Approval.ApprovalStatus getStatus() {
        return status;
    }

    public void setStatus(Approval.ApprovalStatus status) {
        this.status = status;
    }

    public Date getExpiresAt() {
        return expiresAt;
    }

    public void setExpiresAt(Date expiresAt) {
        this.expiresAt = expiresAt;
    }

    public Date getLastUpdatedAt() {
        return lastUpdatedAt;
    }

    public void setLastUpdatedAt(Date lastUpdatedAt) {
        this.lastUpdatedAt = lastUpdatedAt;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Approve approve = (Approve) o;
        return Objects.equals(user, approve.user) &&
                Objects.equals(client, approve.client) &&
                Objects.equals(scope, approve.scope) &&
                status == approve.status;
    }

    @Override
    public int hashCode() {
        return Objects.hash(user, client, scope, status);
    }
}
