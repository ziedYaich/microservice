package com.softtodo.auth.controller;

import com.softtodo.auth.entities.Scope;
import com.softtodo.core.controllers.MongoGenericController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("scope")
public class ScopeController extends MongoGenericController<Scope> {
}
