package com.example.test.search;

import com.example.test.service.dto.Structure;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.ParameterExpression;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.metamodel.Attribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.Type;
import java.util.UUID;

public class InCriteria implements Criteria
{

    private Object value;
    private Attribute attribute;
    private String id;

    public InCriteria(String field, Object value, Structure structure) {
        this.value = value;
        this.attribute = structure.getAttribute(field).getAttribute();
        this.id = "id_"+UUID.randomUUID().toString().replaceAll("-", "_");
    }

    @Override
    public Predicate toPredicate(Root root, CriteriaQuery query, CriteriaBuilder builder) {
        if (!(this.attribute instanceof SingularAttribute)) {
            throw new IllegalArgumentException("Operator 'IN' is applicacable only for singular attributes");
        }
        SingularAttribute singularAttribute = (SingularAttribute) this.attribute;
        Path path = root.get(singularAttribute);
        ParameterExpression parameter = builder.parameter(singularAttribute.getJavaType(), this.id);
        return path.in(parameter);
    }

    @Override
    public void process(TypedQuery query) {
        query.setParameter(this.id,this.value);
    }
}
