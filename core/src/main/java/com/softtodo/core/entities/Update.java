package com.softtodo.core.entities;


import java.util.Date;

public class Update {
    private String clientId;
    private String username;
    private boolean client;
    private Date date;
    private UpdateType type;


    public Update(UpdateType type) {
        /*this.client = SecurityUtils.currentOAuth2Authentication().isClientOnly();
        this.username = this.client ? SecurityUtils.currentClientId() : SecurityUtils.currentUsername();
        this.clientId = SecurityUtils.currentClientId() ;
        this.type = type;
        this.date = new Date();*/
    }

    public String getClientId() { return clientId; }

    public void setClientId(String clientId) { this.clientId = clientId; }

    public String getUsername() { return username; }

    public void setUsername(String username) { this.username = username; }

    public boolean isClient() { return client; }

    public void setClient(boolean client) { this.client = client; }

    public Date getDate() { return date; }

    public void setDate(Date date) { this.date = date; }

    public UpdateType getType() { return type; }

    public void setType(UpdateType type) { this.type = type; }

    public enum UpdateType {
        CREATE, UPDATE, DELETE
    }
}
