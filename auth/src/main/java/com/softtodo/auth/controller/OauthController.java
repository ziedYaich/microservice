package com.softtodo.auth.controller;

import com.softtodo.auth.entities.Client;
import com.softtodo.auth.entities.Scope;
import com.softtodo.auth.entities.User;
import com.softtodo.auth.repository.ClientRepository;
import com.softtodo.auth.repository.ScopeRepository;
import com.softtodo.auth.service.ClientService;
import com.softtodo.auth.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.oauth2.provider.AuthorizationRequest;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.web.servletapi.SecurityContextHolderAwareRequestWrapper;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.SessionAttributes;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping
@SessionAttributes("authorizationRequest")
public class OauthController {
    @Autowired
    private ScopeRepository scopeRepository;


    @Autowired
    private UserService userService;
    @Autowired
    private ClientService clientService;

    @RequestMapping("/oauth/confirm_access")
    public Map<String, Object> getAccessConfirmation(Map<String, Object> model, HttpServletRequest request) {
        Map<String, Object> res = new HashMap<>();
        res.put("client", createClient(model));
        res.put("scope", createScopes(model));
        return res;
    }


    @RequestMapping(path = "/users/me", method = RequestMethod.GET)
    public Object me(OAuth2Authentication authentication) {
        if (authentication.isClientOnly()) {
            return clientService.findByClientId(authentication.getName());
        }
        return userService.findByUsername(authentication.getName());
    }

    @RequestMapping(value = "/users/exists", method = RequestMethod.GET)
    public Map<String,Object> existsMail(@RequestParam(name = "email") String email) {
        User user = userService.findByUsername(email);
        Map<String,Object> res = new HashMap<>();
        res.put("value",user != null ? user.getUsername():false);
        return res;
    }


    private List<Scope> createScopes(Map<String, Object> model) {
        Object o = model.get("authorizationRequest");
        if (o != null && o instanceof org.springframework.security.oauth2.provider.AuthorizationRequest) {
            org.springframework.security.oauth2.provider.AuthorizationRequest authorizationRequest = (org.springframework.security.oauth2.provider.AuthorizationRequest) o;
            return scopeRepository.findByNameIn(authorizationRequest.getScope());
        }
        return Collections.emptyList();
    }

    private Client createClient(Map<String, Object> model) {
        Object o = model.get("authorizationRequest");
        if (o != null && o instanceof AuthorizationRequest) {
            AuthorizationRequest authorizationRequest = (AuthorizationRequest) o;
            return clientService.findByClientId(authorizationRequest.getClientId());
        }
        return null;
    }
}
