package com.softtodo.core.services;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import com.softtodo.core.entities.BaseEntity;
import com.softtodo.core.entities.helpers.EntityStructure;
import com.softtodo.core.entities.helpers.FieldStructure;
import com.softtodo.core.response.ResponseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.repository.query.MongoEntityInformation;
import org.springframework.data.mongodb.repository.support.MongoRepositoryFactory;
import org.springframework.data.util.ClassTypeInformation;
import org.springframework.data.util.TypeInformation;
import org.springframework.util.Assert;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MongoGenericServiceHolder {
    private static Map<Class<? extends BaseEntity>, MongoGenericService> serviceMap = new HashMap<>();

    public static void subscribe(Class<? extends BaseEntity> clazz, MongoGenericService service) {
        serviceMap.put(clazz, service);
    }

    public static MongoGenericService get(Class<? extends BaseEntity> clazz) {
        MongoGenericService service = serviceMap.get(clazz);
        if (service == null) {
            throw new ResponseException("Unknown Error", 500);
        }
        return service;
    }
}
