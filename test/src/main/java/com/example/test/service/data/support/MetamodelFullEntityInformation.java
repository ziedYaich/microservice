package com.example.test.service.data.support;

import com.example.test.service.dto.Structure;
import org.springframework.data.jpa.repository.support.JpaMetamodelEntityInformation;

import javax.persistence.metamodel.EntityType;
import javax.persistence.metamodel.ManagedType;
import javax.persistence.metamodel.Metamodel;
import javax.persistence.metamodel.Type;
import java.io.Serializable;

public class MetamodelFullEntityInformation<T, ID extends Serializable> extends JpaMetamodelEntityInformation<T, ID>{

    private final Metamodel metamodel;
    private final ManagedType<T> managedType;

    public MetamodelFullEntityInformation(Class<T> domainClass, Metamodel metamodel) {
        super(domainClass, metamodel);
        this.metamodel=metamodel;
        this.managedType = metamodel.managedType(domainClass);
    }

    public ManagedType<T> getManagedType() {
        return managedType;
    }
}
