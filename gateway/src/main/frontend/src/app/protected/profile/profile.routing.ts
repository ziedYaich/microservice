import {Routes} from '@angular/router';

import {ProfileComponent} from './profile.component';
import {UserComponent} from "../user/user.component";
import {StarterComponent} from "../starter/starter.component";

export const ProfileRoutes: Routes = [


      {
        path: 'profile',
        component: ProfileComponent
      }


  ];
