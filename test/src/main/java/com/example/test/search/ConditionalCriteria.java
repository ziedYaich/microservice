package com.example.test.search;

import com.example.test.service.dto.Structure;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.ParameterExpression;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.metamodel.Attribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.Type;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class ConditionalCriteria implements Criteria {

    private ConditionalOperator operator;
    private Object value;
    private Attribute attribute;
    private String id;

    public ConditionalCriteria(ConditionalOperator operator, String field, Object value, Structure structure) {
        this.operator = operator;
        this.attribute = structure.getAttribute(field).getAttribute();
        this.id = "id_" + UUID.randomUUID().toString().replaceAll("-", "_");
        this.value = value;
    }

    @SuppressWarnings("unchecked")
    @Override
    public Predicate toPredicate(Root root, CriteriaQuery query, CriteriaBuilder builder) {
        if (!(this.attribute instanceof SingularAttribute)) {
            throw new IllegalArgumentException(String.format("Operator %s is applicable only for singular attributes", this.operator));
        }
        SingularAttribute singularAttribute = (SingularAttribute) this.attribute;
        if (!singularAttribute.getType().getPersistenceType().equals(Type.PersistenceType.BASIC)) {
            throw new IllegalArgumentException(String.format("Operator %s is applicable only for basic attributes", this.operator));
        }


        Path path = root.get(singularAttribute);
        Class javaType = singularAttribute.getJavaType();
        if(javaType.isPrimitive()){
            javaType = cast(javaType);
        }
        ParameterExpression parameter = builder.parameter(javaType, this.id);
        switch (operator) {
            case GREATER:
                return builder.greaterThan(path, parameter);
            case LOWER:
                return builder.lessThan(path, parameter);
            case GREATER_OR_EQUAL:
                return builder.greaterThanOrEqualTo(path, parameter);
            case LOWER_OR_EQUAL:
                return builder.lessThanOrEqualTo(path, parameter);
            case EQUAL:

                return builder.equal(path, parameter);
            case LIKE:
                if (!CharSequence.class.isAssignableFrom(javaType)) {
                    throw new IllegalArgumentException("Operator LIKE is applicable only for String values");
                }
                return builder.like(path, parameter);
        }
        return null;
    }

    @Override
    public void process(TypedQuery query) {
        try {
            query.setParameter(this.id,this.value);
        } catch (Exception e) {
            throw new IllegalArgumentException(e);
        }
    }

    private Class cast(Class primitive){
        return map.get(primitive);
    }

    public enum ConditionalOperator {
        GREATER,
        LOWER,
        GREATER_OR_EQUAL,
        LOWER_OR_EQUAL,
        EQUAL,
        LIKE
    }

    private static final Map<Class, Class> map = new HashMap<>(9);
    static {
        map.put(boolean.class, Boolean.class);
        map.put(char.class, Character.class);
        map.put(byte.class, Byte.class);
        map.put(short.class, Short.class);
        map.put(int.class, Integer.class);
        map.put(long.class, Long.class);
        map.put(float.class, Float.class);
        map.put(double.class, Double.class);
        map.put(void.class, Void.class);
    }
}
