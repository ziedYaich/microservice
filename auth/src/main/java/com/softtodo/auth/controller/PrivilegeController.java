package com.softtodo.auth.controller;

import com.softtodo.auth.entities.Privilege;
import com.softtodo.core.controllers.MongoGenericController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("privilege")
public class PrivilegeController extends MongoGenericController<Privilege> {
}
