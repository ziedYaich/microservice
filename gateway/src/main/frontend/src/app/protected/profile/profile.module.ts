import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule} from "@angular/router";
import {FlexLayoutModule} from "@angular/flex-layout";
import {DemoMaterialModule} from "../../demo-material-module";

import {FormsModule, FormGroup, ReactiveFormsModule} from '@angular/forms';

import {ProfileComponent} from "./profile.component";
import {ProfileRoutes} from "./profile.routing";
import {TranslateModule} from "@ngx-translate/core";


@NgModule({
  imports: [
    CommonModule,
    DemoMaterialModule,
    FlexLayoutModule,
    RouterModule.forChild(ProfileRoutes),
    FormsModule,
    ReactiveFormsModule,
    TranslateModule
  ],
  entryComponents: [
  ],
  declarations: [
    ProfileComponent
  ]
})
export class ProfileModule { }
