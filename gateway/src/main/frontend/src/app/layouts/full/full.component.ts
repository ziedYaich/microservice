import {MediaMatcher} from '@angular/cdk/layout';
import {AfterViewInit, ChangeDetectorRef, Component, OnDestroy} from '@angular/core';
import {MenuItems} from '../../shared/menu-items/menu-items';

import { Router } from '@angular/router';
import {TranslateService} from "@ngx-translate/core";
/** @title Responsive sidenav */
@Component({
  selector: 'app-full-layout',
  templateUrl: 'full.component.html',
  styleUrls: [],
})
export class FullComponent implements OnDestroy, AfterViewInit {

      mobileQuery: MediaQueryList;
      private _mobileQueryListener: () => void;

      constructor(private translate: TranslateService,private  router : Router,changeDetectorRef: ChangeDetectorRef, media: MediaMatcher, public menuItems: MenuItems) {
        this.mobileQuery = media.matchMedia('(min-width: 768px)');
        this._mobileQueryListener = () => changeDetectorRef.detectChanges();
        this.mobileQuery.addListener(this._mobileQueryListener);
      }


      onNotify(message:string):void {
      }

      ngOnDestroy(): void {
        this.mobileQuery.removeListener(this._mobileQueryListener);
      }

       ngAfterViewInit() {}


}
