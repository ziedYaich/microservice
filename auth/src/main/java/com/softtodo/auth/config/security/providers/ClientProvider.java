package com.softtodo.auth.config.security.providers;

import com.softtodo.auth.dto.DefaultClientDetails;
import com.softtodo.auth.entities.Client;
import com.softtodo.auth.repository.ClientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.oauth2.provider.ClientDetails;
import org.springframework.security.oauth2.provider.ClientDetailsService;
import org.springframework.security.oauth2.provider.ClientRegistrationException;
import org.springframework.stereotype.Component;

@Component
public class ClientProvider implements ClientDetailsService {
    @Autowired
    private ClientRepository clientRepository;

    @Override
    public ClientDetails loadClientByClientId(String clientId) throws ClientRegistrationException {
        Client client = clientRepository.findByClientId(clientId);
        if (client == null) {
            throw new ClientRegistrationException(clientId);
        }
        return new DefaultClientDetails(client, null, null);
    }
}