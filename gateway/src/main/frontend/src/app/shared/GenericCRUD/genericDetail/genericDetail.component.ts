import {AfterViewInit, Component, EventEmitter, Inject, Input, Output, ViewChild} from '@angular/core';

@Component({
  selector: 'generic-detail',
  templateUrl: 'generic-detail.html',
  styleUrls: ['generic-detail.css']
})
export class GenericDetail {


    @Input() ItemDetail;
    keys: any;


    constructor() {}

      ngOnInit(): void {
        this.keys=Object.keys(this.ItemDetail);
    }


}

