package com.softtodo.core.entities.helpers;

import com.softtodo.core.entities.BaseEntity;
import com.softtodo.core.response.ResponseException;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import java.io.IOException;
import java.lang.reflect.Field;
import java.util.HashSet;
import java.util.Objects;
import java.util.Properties;
import java.util.Set;
import java.util.function.Consumer;

public class EntityStructure<T extends BaseEntity> {
    private String name;
    private String singularDisplayName;
    private String pluralDisplayName;
    private Set<FieldStructure> fields = new HashSet<>();
    private Class<T> type;

    EntityStructure(Class<T> type) {
        this.type = type;
        Resource resource = new ClassPathResource(String.format("entities/%s", type.getName().replaceAll("\\.", "\\/")));
        try {
            Properties properties = new Properties();
            properties.load(resource.getInputStream());
            resolve(properties);
        } catch (IOException e) {
            resolve(null);
        }
    }

    public T newInstance() {
        try {
            return this.type.newInstance();
        } catch (InstantiationException | IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSingularDisplayName() {
        return singularDisplayName;
    }

    public void setSingularDisplayName(String singularDisplayName) {
        this.singularDisplayName = singularDisplayName;
    }

    public String getPluralDisplayName() {
        return pluralDisplayName;
    }

    public void setPluralDisplayName(String pluralDisplayName) {
        this.pluralDisplayName = pluralDisplayName;
    }

    public Set<FieldStructure> getFields() {
        return fields;
    }

    public void setFields(Set<FieldStructure> fields) {
        this.fields = fields;
    }

    public FieldStructure getFieldStructure(String name) {
        return fields.stream().filter(fieldStructure -> fieldStructure.getName().equals(name))
                .findFirst()
                .orElseThrow(() -> new ResponseException(String.format("Invalid field %s", name), 400));
    }

    private void resolve(Properties properties) {
        resolveNames(properties);
        resolveFields(properties);
    }

    private void resolveFields(Properties properties) {
        Class<?> currentClass = this.type;
        while (!Objects.equals(currentClass, Object.class)) {
            Field[] fields = currentClass.getDeclaredFields();
            for (Field field : fields) {
                this.fields.add(new FieldStructure(this, field, properties));
            }
            currentClass = currentClass.getSuperclass();
        }
    }

    private void resolveNames(Properties properties) {
        resolveName(properties);
        resolveSingularDisplayName(properties);
        resolvePluralDisplayName(properties);
    }

    private void resolveName(Properties properties) {
        if (properties != null) {
            this.name = properties.getProperty("name");
        }
        if (this.name == null) {
            String className = this.type.getSimpleName();
            StringBuilder builder = new StringBuilder();
            for (int i = 0; i < className.length(); i++) {
                if (Character.isUpperCase(className.charAt(i))) {
                    builder.append("-");
                }
                builder.append(Character.toLowerCase(className.charAt(i)));
            }
            this.name = builder.toString();
        }
    }

    private void resolveSingularDisplayName(Properties properties) {
        if (properties != null) {
            this.singularDisplayName = properties.getProperty("singular");
        }
        if (this.singularDisplayName == null) {
            String className = this.type.getSimpleName();
            StringBuilder builder = new StringBuilder();
            for (int i = 0; i < className.length(); i++) {
                if (Character.isUpperCase(className.charAt(i))) {
                    builder.append(" ");
                }
                builder.append(className.charAt(i));
            }
            this.singularDisplayName = builder.toString();
        }
    }

    public void forEach(Consumer<? super FieldStructure> action) {
        fields.forEach(action);
    }

    private void resolvePluralDisplayName(Properties properties) {
        if (properties != null) {
            this.pluralDisplayName = properties.getProperty("plural");
        }
        if (this.pluralDisplayName == null) {
            this.pluralDisplayName = this.singularDisplayName + "s";
        }
    }

    public boolean canDelete(T entity) {
        return true;
    }

    public boolean canDeleteCompletely(T entity) {
        return true;
    }
}