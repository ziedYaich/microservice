package com.softtodo.auth.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.softtodo.core.entities.BaseEntity;
import org.apache.commons.lang.RandomStringUtils;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Collection;
import java.util.HashSet;
import java.util.UUID;

@Document
public class Client extends BaseEntity {

    private String clientId = UUID.randomUUID().toString();

    private String name;

    private String description;

    @JsonIgnore
    @org.codehaus.jackson.annotate.JsonIgnore
    private String secret = RandomStringUtils.randomAlphanumeric(15);

    @DBRef
    private Collection<Scope> scopes = new HashSet<>();

    @DBRef
    private Collection<Resource> resources = new HashSet<>();

    @JsonIgnore
    @org.codehaus.jackson.annotate.JsonIgnore
    private boolean secretRequired = true;

    @JsonIgnore
    @org.codehaus.jackson.annotate.JsonIgnore
    private boolean scoped = true;

    @JsonIgnore
    @org.codehaus.jackson.annotate.JsonIgnore
    private String redirectUri;

    @JsonIgnore
    @org.codehaus.jackson.annotate.JsonIgnore
    @DBRef
    private Collection<Role> roles = new HashSet<>();

    @JsonIgnore
    @org.codehaus.jackson.annotate.JsonIgnore
    @DBRef
    private Collection<Approve> approves = new HashSet<>();

    @JsonIgnore
    @org.codehaus.jackson.annotate.JsonIgnore
    private ClientType type;
    private Integer tokenValidity;

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getSecret() {
        return secret;
    }

    public void setSecret(String secret) {
        this.secret = secret;
    }

    public Collection<Scope> getScopes() {
        return scopes;
    }

    public void setScopes(Collection<Scope> scopes) {
        this.scopes = scopes;
    }

    public Collection<Resource> getResources() {
        return resources;
    }

    public void setResources(Collection<Resource> resources) {
        this.resources = resources;
    }

    public boolean isSecretRequired() {
        return secretRequired;
    }

    public void setSecretRequired(boolean secretRequired) {
        this.secretRequired = secretRequired;
    }

    public boolean isScoped() {
        return scoped;
    }

    public void setScoped(boolean scoped) {
        this.scoped = scoped;
    }

    public String getRedirectUri() {
        return redirectUri;
    }

    public void setRedirectUri(String redirectUri) {
        this.redirectUri = redirectUri;
    }

    public Collection<Role> getRoles() {
        return roles;
    }

    public void setRoles(Collection<Role> roles) {
        this.roles = roles;
    }

    public Collection<Approve> getApproves() {
        return approves;
    }

    public void setApproves(Collection<Approve> approves) {
        this.approves = approves;
    }

    public ClientType getType() {
        return type;
    }

    public void setType(ClientType type) {
        this.type = type;
    }

    public Integer getTokenValidity() {
        return tokenValidity;
    }

    public void setTokenValidity(Integer tokenValidity) {
        this.tokenValidity = tokenValidity;
    }

    public enum ClientType {
        SERVER, USER_GUEST, CLIENT_GUEST
    }
}
