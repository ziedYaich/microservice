package com.example.test.search;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.ParameterExpression;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Root;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.Type;
import java.util.List;
import javax.persistence.criteria.Predicate;
import java.util.stream.Collectors;

public class LogicalCriteria implements Criteria
{
    Criteria[] operands;
    LogicalOperator operator;

    public LogicalCriteria(Criteria[] operands, LogicalOperator operator) {
        this.operands = operands;
        this.operator = operator;
    }

    @Override
    public Predicate toPredicate(Root root, CriteriaQuery query, CriteriaBuilder builder) {
        Predicate[] predicates = new Predicate[operands.length];
        for (int i = 0 ; i < operands.length ; i++){
            predicates[i] = operands[i].toPredicate(root, query, builder);
        }
        switch (operator) {
            case AND:
                return builder.and(predicates);
            case OR:
                return builder.or(predicates);
        }
        return null;
    }

    @Override
    public void process(TypedQuery query) {
        for (Criteria operand : operands) {
            operand.process(query);
        }
    }

    public enum LogicalOperator{
        AND,OR
    }
}
