package com.softtodo.core.response;

public class ResponseException extends RuntimeException {
    private int code ;
    private Object[] args ;
    public ResponseException(String message, int code,Object... args) {
        super(message);
        this.args = args;
        this.code = code;
    }

    public int getCode() {
        return code;
    }

    public Object[] getArgs() {
        return args;
    }
}
