package com.example.test.service.dto;

import com.example.test.service.StructureFactory;
import org.springframework.util.Assert;

import javax.persistence.metamodel.Attribute;
import javax.persistence.metamodel.ManagedType;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

public class Structure<T> {
    private String name;
    private boolean mappedSuperClass;
    private ManagedType<T> managedType;
    private Set<AttributeField> attributes;


    @SuppressWarnings("unchecked")
    public Structure(StructureFactory structureFactory, ManagedType managedType) {
        Set<Attribute> attrs = managedType.getAttributes();
        this.managedType = managedType;
        this.attributes = attrs.stream().map(attribute -> new AttributeField(structureFactory, attribute)).collect(Collectors.toSet());
    }

    public ManagedType<T> getManagedType() {
        return managedType;
    }

    public AttributeField getAttribute(String name) {
        Assert.notNull(name, "the name of attribute can't be null");
        int firstIndexOfPoint = name.indexOf('.');
        if (firstIndexOfPoint < 0) {
            return attributes.stream()
                    .filter(attributeField -> Objects.equals(name, attributeField.getName()))
                    .findFirst()
                    .orElseThrow(() -> new IllegalArgumentException(String.format("Invalid field (ends with '.') %s", name)));
        }
        if (firstIndexOfPoint == name.length() - 1) {
            throw new IllegalArgumentException(String.format("Invalid field (ends with '.') %s", name));
        }
        String attrName = name.substring(0, firstIndexOfPoint);
        String subAttrName = name.substring(1 + firstIndexOfPoint);

        AttributeField attribute = attributes.stream()
                .filter(attributeField -> Objects.equals(attrName, attributeField.getName()))
                .findFirst()
                .orElse(null);
        Assert.notNull(attribute, String.format("Invalid field %s", name));
        Assert.notNull(attribute.getStructure(), String.format("Invalid field %s", name));
        return attribute.getStructure().getAttribute(subAttrName);
    }

    public Set<AttributeField> getAttributes() {
        return attributes;
    }

    public String getName() {
        return name;
    }

    public boolean isMappedSuperClass() {
        return mappedSuperClass;
    }

    public void setAttributes(Set<AttributeField> attributes) {
        this.attributes = attributes;
    }

    public void setMappedSuperClass(boolean mappedSuperClass) {
        this.mappedSuperClass = mappedSuperClass;
    }

    public void setName(String name) {
        this.name = name;
    }


}
