package com.softtodo.auth.service;

import com.softtodo.auth.entities.User;
import com.softtodo.core.services.MongoGenericService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserService extends MongoGenericService<User> {
    @Autowired
    private PasswordEncoder passwordEncoder;

    public User encodePassword(User user) {
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        return user;
    }

    public User findByUsername(String username) {
        return super.findOneBy("username",username);
    }
}
