import {Injectable} from '@angular/core';



export interface Interfacechildmenu {

      stateChild:string;
      name:string;
      icon:string;


}
export interface Menu {

  name: string;
  state: string;
  type: string;
  icon: string;
  childmenu:Interfacechildmenu[];

}

const MENUITEMS = [
  {
    name: 'Dashboard',
    state: 'starter',
    type: 'link',
    icon: 'dashboard' ,
    childmenu: null
  },
  {
    name: 'user.name',
    state: 'user',
    type: 'link',
    icon: 'supervisor_account',
    childmenu:
      [
        {stateChild: 'add', name: 'user.add',   icon: 'add' },
        {stateChild: 'list', name: 'user.list',  icon: 'view_list' }
      ]
  },
  {
    name: 'product.name',
    state: 'product',
    type: 'link',
    icon: 'shopping_cart',
    childmenu:
      [
        {stateChild: 'add', name: 'product.add',   icon: 'add' },
        {stateChild: 'list', name: 'product.list',  icon: 'view_list' }
      ]
  },

  {
    name: 'vendor.name',
    state: 'product',
    type: 'link',
    icon: 'shopping_cart',
    childmenu:
      [
        {stateChild: 'add', name: 'vendor.add',   icon: 'add' },
        {stateChild: 'list', name: 'vendor.list',  icon: 'view_list' }
      ]
  },
  {
    name: 'invoice.name',
    state: 'invoice',
    type: 'link',
    icon: 'chrome_reader_mode',
    childmenu:
      [
        {stateChild: 'invoice', name: 'invoice.invoices',   icon: 'assignment' },
        {stateChild: 'invoice', name: 'invoice.orders',  icon: 'assignment_turned_in' },
        {stateChild: 'invoice', name: 'invoice.delivery_notes',  icon: 'view_list' },
        {stateChild: 'invoice', name: 'invoice.exit_notes',  icon: 'assignment_return' },
        {stateChild: 'invoice', name: 'invoice.estimates',  icon: 'view_list' },
      ]
  },
  {
    name: 'Expansion Panel',
    state: 'expansion',
    type: 'link',
    icon: 'business_center',
    childmenu: null
  },
  {
    name: 'New Invoice',
    state: 'newinvoice',
    type: 'link',
    icon: 'note_add',
    childmenu: null
  }


    // {state: 'button', type: 'link', name: 'Buttons', icon: 'crop_7_5'},
    // {state: 'grid', type: 'link', name: 'Grid List', icon: 'view_comfy'},
    // {state: 'lists', type: 'link', name: 'Lists', icon: 'view_list'},
    // {state: 'menu', type: 'link', name: 'Menu', icon: 'view_headline'},
    // {state: 'tabs', type: 'link', name: 'Tabs', icon: 'tab'},
    // {state: 'stepper', type: 'link', name: 'Stepper', icon: 'web'},
    // {state: 'chips', type: 'link', name: 'Chips', icon: 'vignette'},
    // {state: 'toolbar', type: 'link', name: 'Toolbar', icon: 'voicemail'},
    // {state: 'viewProgress-snipper', type: 'link', name: 'Progress snipper', icon: 'border_horizontal'},
    // {state: 'viewProgress', type: 'link', name: 'Progress Bar', icon: 'blur_circular'},
    // {state: 'dialog', type: 'link', name: 'Dialog', icon: 'assignment_turned_in'},
    // {state: 'tooltip', type: 'link', name: 'Tooltip', icon: 'assistant'},
    // {state: 'snackbar', type: 'link', name: 'Snackbar', icon: 'adb'},
    // {state: 'slider', type: 'link', name: 'Slider', icon: 'developer_mode'},
    // {state: 'slide-toggle', type: 'link', name: 'Slide Toggle', icon: 'all_inclusive'},

];

@Injectable()

export class MenuItems {
  getMenuitem(): Menu[] {
    return MENUITEMS;
  }

}
