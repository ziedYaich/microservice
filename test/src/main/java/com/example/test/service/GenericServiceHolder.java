package com.example.test.service;

import com.example.test.BaseEntity;
import com.example.test.GenericService;
import com.example.test.service.dto.Structure;
import org.springframework.beans.factory.annotation.Autowired;
import sun.reflect.CallerSensitive;
import sun.reflect.Reflection;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import java.util.HashMap;
import java.util.Map;

public class GenericServiceHolder {
    private static Map<Class,GenericService> serviceMap = new HashMap<>();

    public static GenericService getServiceForClass(Class<? extends BaseEntity> domain){
        return serviceMap.get(domain);
    }

    @CallerSensitive
    public static void subscribe(Class<? extends BaseEntity> domain , GenericService service){
//        System.out.println(Reflection.getCallerClass());
        serviceMap.put(domain,service);
    }
}
