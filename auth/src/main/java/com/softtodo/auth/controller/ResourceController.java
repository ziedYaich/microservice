package com.softtodo.auth.controller;

import com.softtodo.auth.entities.Resource;
import com.softtodo.core.controllers.MongoGenericController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("resource")
public class ResourceController extends MongoGenericController<Resource> {
}
