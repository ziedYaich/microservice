package com.softtodo.auth.controller;

import com.softtodo.auth.entities.Role;
import com.softtodo.core.controllers.MongoGenericController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("role")
public class RoleController extends MongoGenericController<Role> {
}
