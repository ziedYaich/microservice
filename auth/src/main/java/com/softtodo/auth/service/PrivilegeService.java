package com.softtodo.auth.service;

import com.softtodo.auth.entities.Privilege;
import com.softtodo.core.services.MongoGenericService;
import org.springframework.stereotype.Service;

@Service
public class PrivilegeService extends MongoGenericService<Privilege> {
}
