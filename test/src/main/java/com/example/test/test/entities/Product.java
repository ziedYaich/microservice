package com.example.test.test.entities;

import com.example.test.BaseEntity;

import javax.persistence.Entity;

@Entity
public class Product extends BaseEntity {
    private String nom;
    private Double price;

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }
}
