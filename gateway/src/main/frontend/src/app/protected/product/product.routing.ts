import {Routes} from '@angular/router';

import {ProductComponent} from './product.component';
import {UserComponent} from "../user/user.component";

export const ProductRoutes: Routes = [

      {
        path: 'product/:rootview',
        component: ProductComponent
      },
      {
        path: 'product',
        redirectTo: '/product/list'
      }

  ];
