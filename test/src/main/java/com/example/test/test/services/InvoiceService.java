package com.example.test.test.services;

import com.example.test.GenericService;
import com.example.test.test.entities.Client;
import com.example.test.test.entities.Invoice;
import org.springframework.stereotype.Service;

@Service
public class InvoiceService extends GenericService<Invoice>{
    
}
