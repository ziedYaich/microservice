package com.softtodo.core.search;

import com.softtodo.core.entities.helpers.EntityStructure;
import com.softtodo.core.entities.helpers.FieldStructure;
import org.springframework.data.mongodb.core.query.Criteria;

public class ConditionalSearchObject implements SearchObject {

    private ConditionalOperator operator;
    private Object value;
    private FieldStructure attribute;

    public ConditionalSearchObject(ConditionalOperator operator, String field, Object value, EntityStructure structure) {
        this.operator = operator;
        this.attribute = structure.getFieldStructure(field);
        this.value = value;
    }

    @Override
    public Criteria toCriteria() {
        switch (operator) {
            case GREATER:
                return org.springframework.data.mongodb.core.query.Criteria
                        .where(this.attribute.getName()).gt(value);
            case LOWER:
                return org.springframework.data.mongodb.core.query.Criteria
                        .where(this.attribute.getName()).lt(value);
            case GREATER_OR_EQUAL:
                return org.springframework.data.mongodb.core.query.Criteria
                        .where(this.attribute.getName()).gte(value);
            case LOWER_OR_EQUAL:
                return org.springframework.data.mongodb.core.query.Criteria
                        .where(this.attribute.getName()).lte(value);
            case EQUAL:
                return org.springframework.data.mongodb.core.query.Criteria
                        .where(this.attribute.getName()).is(value);
            case IN:
                return org.springframework.data.mongodb.core.query.Criteria
                        .where(this.attribute.getName()).in(value);
            case LIKE:
                return org.springframework.data.mongodb.core.query.Criteria
                        .where(this.attribute.getName()).is(value);
        }
        return null;
    }


    public enum ConditionalOperator {
        GREATER,
        LOWER,
        GREATER_OR_EQUAL,
        LOWER_OR_EQUAL,
        EQUAL,
        LIKE,
        IN
    }
}
