package com.softtodo.core.search;

import org.springframework.data.mongodb.core.query.Criteria;

public class LogicalSearchObject implements SearchObject
{
    private SearchObject[] operands;
    private LogicalOperator operator;

    public LogicalSearchObject(SearchObject[] operands, LogicalOperator operator) {
        this.operands = operands;
        this.operator = operator;
    }

    @Override
    public Criteria toCriteria() {
        Criteria criteria = new Criteria();
        Criteria[] tab = new Criteria[operands.length];
        for (int i = 0; i < tab.length; i++) {
            tab[i] = operands[i].toCriteria();
        }
        switch (operator) {
            case AND:
                return criteria.andOperator(tab);
            case OR:
                return criteria.orOperator(tab);
        }
        return criteria;

    }

    public enum LogicalOperator{
        AND,OR
    }
}
