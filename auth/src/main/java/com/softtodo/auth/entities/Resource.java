package com.softtodo.auth.entities;

import com.softtodo.core.entities.BaseEntity;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Objects;

@Document
public class Resource extends BaseEntity{

    private String resourceId;

    private String description;

    public String getResourceId() {
        return resourceId;
    }

    public void setResourceId(String resourceId) {
        this.resourceId = resourceId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Resource scope = (Resource) o;
        return Objects.equals(resourceId, scope.resourceId);
    }

    @Override
    public int hashCode() {

        return Objects.hash(resourceId);
    }

}
