import {Routes} from '@angular/router';

import {InvoiceRowEdition} from "./invoice-row-edition.component";

export const NewInvoiceRoutes: Routes = [


      {
        path: 'newinvoice',
        component: InvoiceRowEdition
      }
  ];
