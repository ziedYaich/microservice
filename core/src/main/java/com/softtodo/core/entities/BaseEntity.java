package com.softtodo.core.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.data.annotation.Id;

import java.io.Serializable;

/**
 * Simple @MappedSuperclass with an id property. Used as a base class for all entities
 *
 */
public abstract class BaseEntity implements Serializable {
    @Id
    private String id;
    @JsonIgnore
    private boolean deleted;

    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }
    @JsonIgnore
    public boolean isDeleted() {
        return deleted;
    }
    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }
}
