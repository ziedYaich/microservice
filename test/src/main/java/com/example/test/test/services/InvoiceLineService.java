package com.example.test.test.services;

import com.example.test.GenericService;
import com.example.test.test.entities.Client;
import com.example.test.test.entities.InvoiceLine;
import org.springframework.stereotype.Service;

@Service
public class InvoiceLineService extends GenericService<InvoiceLine>{
    
}
