
import {NgModule} from '@angular/core';

import {DemoMaterialModule} from "../../demo-material-module";
import {HttpModule} from "@angular/http";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {FlexLayoutModule} from "@angular/flex-layout";
import {CommonModule} from "@angular/common";
import {CdkTableModule} from "@angular/cdk/table";


/*All Generics components */
import {GenericDataGrid} from "./genericDataGrid/generic.datagrid.component";
import {GenericDetail} from "./genericDetail/genericDetail.component";
import {GenericAdd} from "./genericAdd/genericAdd.component";
import {GenericDataTable} from "./genericDataTable/dataTable.component";
import {ModalRemove} from "./genericDialogRemove/modalRemove.component";

import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import {TranslateModule} from "@ngx-translate/core";


@NgModule({
  imports: [
    CommonModule,
    DemoMaterialModule,
    HttpModule,
    FormsModule,
    ReactiveFormsModule,
    FlexLayoutModule,
    CdkTableModule,
    InfiniteScrollModule,
    TranslateModule,

  ],
  providers: [

  ],
  entryComponents:[ModalRemove],
  declarations:  [
    ModalRemove,
    GenericDataTable,
    GenericAdd,
    GenericDetail,
    GenericDataGrid
  ],
  exports:  [

    ModalRemove,
    GenericDataTable,
    GenericAdd,
    GenericDetail,
    GenericDataGrid
  ]
})

export class GenericModule {}
