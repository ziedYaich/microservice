package com.softtodo.core.entities;


/**
 * Super mapped class for entities that belong to a company and have a create and last update fields.
 *
 * @author Khaled Baklouti
 */
public abstract class MetaEntity extends BaseEntity {

    private String companyId;
    private Update lastUpdate;
    private Update create = new Update(Update.UpdateType.CREATE);


    public String getCompanyId() {
        return companyId;
    }
    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }
    public Update getLastUpdate() {
        return lastUpdate;
    }
    public Update getCreate() {
        return create;
    }
    public void setLastUpdate(Update lastUpdate) {
        this.lastUpdate = lastUpdate;
    }
    public void setCreate(Update create) {
        this.create = create;
    }

    //@PrePersist
    //private void prePersist(){
       // this.lastUpdate = new Update(Update.UpdateType.UPDATE);
    //}

}
