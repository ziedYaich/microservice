import {AfterViewInit, Component, EventEmitter, Inject, Input, OnInit, Output, ViewChild} from '@angular/core';

import {  MatPaginator, MatSort, MatTableDataSource,
  MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import {FormBuilder, FormGroup, FormControl, Validators} from '@angular/forms';

@Component({
  selector: 'generic-datatable',
  templateUrl: 'generic-dataTable.html',
  styleUrls: ['generic-dataTable.css']
})
export class GenericDataTable implements AfterViewInit,OnInit{

      /* Input to set a data in DataTable*/
      @Input() Directive_DATA;
      /* Input to set a Structure in DataTable*/
      @Input() Structure_Fields;

      /* Output For Action (Edit / Remove) */
      @Output() notifyAction: EventEmitter<[any,string]> = new EventEmitter<[any,string]>();

      @ViewChild(MatPaginator) paginator: MatPaginator;
      @ViewChild(MatSort) sort: MatSort;

      displayedColumns = [];
      dataSource: MatTableDataSource<any>;
      titleComponent:any;

      constructor(public dialog: MatDialog) {}

      ngOnInit(): void {

        this.titleComponent=this.Structure_Fields.name+".list";

        for (let item of this.Structure_Fields.fields) {
            if(item.type.showDataTabel)
            this.displayedColumns.push(item.displayName)
        }

        this.displayedColumns.push('actions');
        console.log(this.displayedColumns);



        const users:any = this.Directive_DATA;
        this.dataSource = new MatTableDataSource(users);
      }

      ngAfterViewInit() {
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      }

      applyFilter(filterValue: string) {
        filterValue = filterValue.trim(); // Remove whitespace
        filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
        this.dataSource.filter = filterValue;
      }

      /* Notify Action Item */
      ActionNotify(item:any,action:string){
        this.notifyAction.emit([item,action]);
      }



}

