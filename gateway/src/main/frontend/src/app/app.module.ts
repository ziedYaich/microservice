import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {HttpClient, HttpClientModule} from '@angular/common/http';
import {HashLocationStrategy, LocationStrategy} from '@angular/common';
import {AppRoutes} from './app.routing';
import {AppComponent} from './app.component';

/* material angular ngflix*/
import {FlexLayoutModule} from '@angular/flex-layout';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';


import {FullComponent} from './layouts/full/full.component';
import {LayoutloginComponent} from "./layouts/layoutlogin/layoutlogin.component";

import {AppHeaderComponent} from './layouts/full/header/header.component';
import {AppSidebarComponent} from './layouts/full/sidebar/sidebar.component';

import {DemoMaterialModule} from './demo-material-module';

import {SharedModule} from './shared/shared.module';
import {SpinnerComponent} from './shared/spinner.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

/*  Public */
import { ErrorComponent } from './public/error/error.component';
import {AuthenticationService} from "./core/services/authentication.service";
import {AuthGuard} from "./shared/guards";

/*  Translate */
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}
@NgModule({
  declarations: [
    AppComponent,
    FullComponent,
    LayoutloginComponent,
    AppHeaderComponent,
    SpinnerComponent,
    AppSidebarComponent,
    ErrorComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    DemoMaterialModule,
    FormsModule,
    FlexLayoutModule,
    HttpClientModule,
    SharedModule ,
    ReactiveFormsModule,
    RouterModule.forRoot(AppRoutes),
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    })


  ],
  providers: [
    AuthGuard,
    AuthenticationService,
  {
    provide: LocationStrategy,
    useClass: HashLocationStrategy,

  }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
