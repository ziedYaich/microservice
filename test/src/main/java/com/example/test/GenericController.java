package com.example.test;

import com.example.test.search.Criteria;
import com.example.test.service.dto.Structure;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

public abstract class GenericController<T extends BaseEntity> {

    @Autowired
    private GenericService<T> service;

    @RequestMapping(value = "structure", method = RequestMethod.GET)
    public Structure<T> getStructure() {
        return service.getStructure();
    }

    @RequestMapping(value = "{id}", method = RequestMethod.GET)
    public T findOne(@PathVariable(name = "id") Long id) {
        return service.findOne(id);
    }

    @RequestMapping(value = "", method = RequestMethod.GET)
    public Page<T> findAll(Pageable pageable) {
        return service.findAll(pageable);
    }

    @RequestMapping(value = "", method = RequestMethod.POST)
    public Page<T> findAll(@RequestBody Criteria criteria, Pageable pageable) {
        return service.findAll(criteria, pageable);
    }

    @RequestMapping(value = "count", method = RequestMethod.GET)
    public long count() {
        return service.count();
    }

    @RequestMapping(value = "count", method = RequestMethod.POST)
    public long count(@RequestBody Criteria criteria) {
        return service.count(criteria);
    }

    @RequestMapping(value = "{id}", method = RequestMethod.PUT)
    public T update(@PathVariable Long id, @RequestBody String node) {
        return service.update(id, node);
    }

    @RequestMapping(value = "", method = RequestMethod.PUT)
    public T save(@RequestBody String node) {
        return service.save(node);
    }
}
