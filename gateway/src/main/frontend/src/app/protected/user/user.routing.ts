import {Routes} from '@angular/router';

import {UserComponent} from './user.component';

export const UserRoutes: Routes = [

      {
        path: 'user/:rootview',
        component: UserComponent
      }
      ,
      {
        path: 'user',
        redirectTo: '/user/list'
      }

  ];
