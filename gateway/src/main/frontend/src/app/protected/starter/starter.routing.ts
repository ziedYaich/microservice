import {Routes} from '@angular/router';

import {StarterComponent} from './starter.component';

export const StarterRoutes: Routes = [
      {
        path: '', redirectTo: 'starter'
      },
      {
        path: 'starter',
        component: StarterComponent
      }
  ];
