package com.softtodo.core.entities.helpers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.softtodo.core.entities.BaseEntity;
import com.softtodo.core.entities.MetaEntity;
import com.softtodo.core.entities.Update;
import com.softtodo.core.entities.annotations.OptionalField;
import com.softtodo.core.entities.annotations.SecurityEditExpression;
import com.softtodo.core.entities.annotations.SecurityReadExpression;
import com.softtodo.core.entities.annotations.Validation;
import com.softtodo.core.response.ResponseException;
import org.springframework.data.annotation.Id;

import java.io.IOException;
import java.lang.reflect.Field;
import java.util.Objects;
import java.util.Properties;
import java.util.Set;

public class FieldStructure<CLASS_TYPE, FIELD_TYPE> {
    private final String name;
    private String displayName;
    private FieldType type;
    private static ObjectMapper objectMapper = new ObjectMapper();
    private EntityStructure entityStructure;

    FieldStructure(EntityStructure entityStructure, Field field, Properties properties) {
        this.entityStructure = entityStructure;
        this.name = field.getName();
        resolve(field, properties);

    }

    private void resolve(Field field, Properties properties) {
        resolveDisplayName(field, properties);
        resolveType(field, properties);
    }

    private void resolveDisplayName(Field field, Properties properties) {
        if (properties != null) {
            this.displayName = properties.getProperty(String.format("fields.%s.name", field.getName()));
        }
        if (this.displayName == null) {
            StringBuilder builder = new StringBuilder(this.name.substring(0, 1));
            for (int i = 1; i < this.name.length(); i++) {
                if (Character.isUpperCase(this.name.charAt(i))) {
                    builder.append(" ");
                }
                builder.append(Character.toLowerCase(this.name.charAt(i)));
            }
            this.displayName = builder.toString();
        }
    }

    private void resolveType(Field field, Properties properties) {
        this.type = new FieldType(field, properties);
    }

    public String getName() {
        return name;
    }

    public boolean isRequired() {
        return this.type.required;
    }

    public boolean isEditable() {
        return evaluateAccess(this.type.canEdit);
    }

    public Class<?> getJavaType() {
        return this.type.field.getType();
    }



    public void setValue(CLASS_TYPE instance, FIELD_TYPE value) {
        Object oldValue = getValueOfInstance(instance);
        if (isChanged(oldValue, value)) {
            if (evaluateAccess(this.type.canEdit)) {
                this.setValueOfInstance(instance, value);
            } else {
                throw new SecurityException(
                        String.format("security.deny.edit.field",
                                this.displayName, this.entityStructure.getSingularDisplayName())
                );
            }
        }
    }
    @SuppressWarnings("unchecked")
    public FIELD_TYPE getValue(CLASS_TYPE instance) {
        if (evaluateAccess(this.type.canRead)) {
            return (FIELD_TYPE) this.getValueOfInstance(instance);
        }
        throw new ResponseException("error.accessDenied.field.read",403,this.displayName);
    }

    @SuppressWarnings("unchecked")
    public FIELD_TYPE getValueSafely(CLASS_TYPE instance) {
        return (FIELD_TYPE) this.getValueOfInstance(instance);
    }

    public void setValueOfInstance(CLASS_TYPE instance, FIELD_TYPE value) {
        boolean accessible = this.type.field.isAccessible();
        this.type.field.setAccessible(true);
        try {
            this.type.field.set(instance, value);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
        this.type.field.setAccessible(accessible);
    }

    private Object getValueOfInstance(CLASS_TYPE instance) {
        boolean accessible = this.type.field.isAccessible();
        this.type.field.setAccessible(true);
        Object response;
        try {
            response = this.type.field.get(instance);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
        this.type.field.setAccessible(accessible);
        return response;
    }

    private boolean isChanged(Object oldValue, FIELD_TYPE value) {
        return (oldValue != null || value != null)
                && (oldValue == null
                || value == null
                || !Objects.equals(oldValue.getClass(), value.getClass())
                || isChanged(oldValue, value, oldValue.getClass()));
    }

    @SuppressWarnings("unchecked")
    private boolean isChanged(Object oldValue, FIELD_TYPE value, Class<?> type) {
        if (BaseEntity.class.isAssignableFrom(type)) {
            EntityStructure structure = StructureBuilder.getClassStructure((Class<? extends BaseEntity>) type);
            Set<FieldStructure> fields = structure.getFields();
            for (FieldStructure fieldStructure : fields){
                if(fieldStructure.isChanged(fieldStructure.getValueOfInstance(oldValue),fieldStructure.getValueOfInstance(value))){
                    return true;
                }
            }
            return false;
        } else {
            return !Objects.equals(oldValue, value);
        }
    }

    public boolean isReadable() {
        return evaluateAccess(this.type.canRead);
    }

    private boolean evaluateAccess(String expressionString) {
        //TODO evaluate security expression
        return true;
    }

    public Integer getMaxLength() {
        return this.type.maxLength;
    }

    public Integer getMinLength() {
        return this.type.minLength;
    }

    public String getRegex() {
        return this.type.regex;
    }

    public Object getMaxValue() {
        return this.type.maxValue;
    }

    public Object getMinValue() {
        return this.type.minValue;
    }

    static class FieldType {
        private Field field;
        private String name;
        private boolean required;
        private String canEdit;
        private String canRead;
        private Integer maxLength;
        private Integer minLength;
        private String regex;
        private Object maxValue;
        private Object minValue;
        private boolean id;
        private boolean update;

        FieldType(Field field, Properties properties) {
            this.field = field;
            this.resolve(properties);
        }

        private void resolve(Properties properties) {
            resolveName(properties);
            this.id = field.isAnnotationPresent(Id.class);
            this.update = field.getType().equals(Update.class) && field.getDeclaringClass().equals(MetaEntity.class);
            this.required = resolveRequired(properties);
            this.canEdit = resolveEditable(properties);
            this.canRead = resolveCanRead(properties);
            resolveValidation(properties);
        }

        private void resolveName(Properties properties) {
            if (properties != null) {
                this.name = properties.getProperty(String.format("fields.%s.type", this.field.getName()));
            }
            if (this.name == null) {
                this.name = this.field.getType().getSimpleName();
            }
        }

        private boolean resolveRequired(Properties properties) {
            String requiredProperty;
            if (properties != null && (requiredProperty = properties.getProperty(String.format("fields.%s.required", this.field.getName()))) != null) {
                if (requiredProperty.equalsIgnoreCase("true")) {
                    return true;
                }
                if (requiredProperty.equalsIgnoreCase("false")) {
                    return false;
                }
            }
            return !this.field.isAnnotationPresent(OptionalField.class);
        }

        private String resolveEditable(Properties properties) {
            String editExpression;
            if (properties != null && (editExpression = properties.getProperty(String.format("fields.%s.edit", field.getName()))) != null) {
                return editExpression;
            }
            if (field.isAnnotationPresent(SecurityEditExpression.class)) {
                return field.getAnnotation(SecurityEditExpression.class).value();
            }
            return "isAuthenticated()";
        }

        private String resolveCanRead(Properties properties) {
            String editExpression;
            if (properties != null && (editExpression = properties.getProperty(String.format("fields.%s.read", this.field.getName()))) != null) {
                return editExpression;
            }
            if (this.field.isAnnotationPresent(SecurityReadExpression.class)) {
                return this.field.getAnnotation(SecurityReadExpression.class).value();
            }
            return "isAuthenticated()";
        }

        private void resolveValidation(Properties properties) {
            Validation validation = field.getAnnotation(Validation.class);
            this.regex = resolveRegex(properties, validation);
            this.minLength = resolveMinLength(properties, validation);
            this.maxLength = resolveMaxLength(properties, validation);
            this.minValue = resolveMinValue(properties, validation);
            this.maxValue = resolveMaxValue(properties, validation);
        }

        private Object resolveMaxValue(Properties properties, Validation validation) {
            String maxValue;
            if (properties != null && (maxValue = properties.getProperty(String.format("fields.%s.maxValue", this.field.getName()))) != null && !maxValue.isEmpty()) {
                return parse(maxValue, "maxValue");
            }
            if (validation != null) {
                maxValue = validation.regex();
                if (!maxValue.isEmpty()) {
                    return parse(maxValue, "maxValue");
                }
            }
            return null;
        }


        private Object resolveMinValue(Properties properties, Validation validation) {
            String minValue;
            if (properties != null && (minValue = properties.getProperty(String.format("fields.%s.minValue", this.field.getName()))) != null && !minValue.isEmpty()) {
                return parse(minValue, "minValue");
            }
            if (validation != null) {
                minValue = validation.regex();
                if (!minValue.isEmpty()) {
                    return parse(minValue, "minValue");
                }
            }
            return null;
        }

        private Integer resolveMaxLength(Properties properties, Validation validation) {
            String maxLength;
            if (properties != null && (maxLength = properties.getProperty(String.format("fields.%s.maxLength", this.field.getName()))) != null && !maxLength.isEmpty()) {
                return parse(maxLength, "maxLength", Integer.class);
            }
            if (validation != null) {
                maxLength = validation.regex();
                if (!maxLength.isEmpty()) {
                    return parse(maxLength, "maxLength", Integer.class);
                }
            }
            return null;
        }

        private Integer resolveMinLength(Properties properties, Validation validation) {
            String minLength;
            if (properties != null && (minLength = properties.getProperty(String.format("fields.%s.minLength", this.field.getName()))) != null && !minLength.isEmpty()) {
                return parse(minLength, "minLength", Integer.class);
            }
            if (validation != null) {
                minLength = validation.regex();
                if (!minLength.isEmpty()) {
                    return parse(minLength, "minLength", Integer.class);
                }
            }
            return null;
        }

        private String resolveRegex(Properties properties, Validation validation) {
            String regex;
            if (properties != null && (regex = properties.getProperty(String.format("fields.%s.regex", this.field.getName()))) != null && !regex.isEmpty()) {
                return regex;
            }
            if (validation != null) {
                regex = validation.regex();
                if (!regex.isEmpty()) {
                    return regex;
                }
            }
            return null;
        }


        private Object parse(String value, String name) {
            return parse(value, name, this.field.getType());
        }

        private <T> T parse(String value, String name, Class<T> type) {
            try {
                return objectMapper.readValue(value, type);
            } catch (IOException e) {
                throw new IllegalArgumentException(
                        String.format("invalid %s (%s) in entity property file %s : %s",
                                name, value, this.field.getDeclaringClass().getSimpleName(), field.getName()));
            }
        }
    }

}