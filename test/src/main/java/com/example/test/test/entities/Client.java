package com.example.test.test.entities;

import com.example.test.BaseEntity;
import com.example.test.annotations.IgnoredAttribute;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Entity
public class Client extends BaseEntity {
    String name;

    @ElementCollection
    List<String> adresses;

    @IgnoredAttribute
    @OneToMany(mappedBy = "client",fetch = FetchType.EAGER)
    @JsonIgnore
    private Collection<Invoice> invoices = new ArrayList<>();


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<String> getAdresses() {
        return adresses;
    }

    public void setAdresses(List<String> adresses) {
        this.adresses = adresses;
    }

    public Collection<Invoice> getInvoices() {
        return invoices;
    }

    public void setInvoices(Collection<Invoice> invoices) {
        this.invoices = invoices;
    }
}
