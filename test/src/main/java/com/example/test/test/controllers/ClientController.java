package com.example.test.test.controllers;

import com.example.test.GenericController;
import com.example.test.GenericService;
import com.example.test.test.entities.Client;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("client")
public class ClientController extends GenericController<Client> {
    
}
