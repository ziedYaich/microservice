import 'hammerjs';
import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {HttpModule} from '@angular/http';
import {CommonModule} from '@angular/common';

import {DemoMaterialModule} from '../demo-material-module';
import {CdkTableModule} from '@angular/cdk/table';

import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {FlexLayoutModule} from '@angular/flex-layout';

import {ProtectedRoutes} from './protected.routing';


import {ExpansionComponent} from './expansion/expansion.component';

import {StarterModule} from "./starter/starter.module";
import { UserModule } from './user/user.module';
import {InvoiceModule} from "./GenericInvoice/invoice.module";
import {ProfileModule} from "./profile/profile.module";
import {ProductModule} from "./product/product.module";

import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import {NewInvoiceModule} from "./invoice-row-edition/invoice-row-edition.module";


@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(ProtectedRoutes),
    DemoMaterialModule,
    HttpModule,
    FormsModule,
    ReactiveFormsModule,
    FlexLayoutModule,
    CdkTableModule,
    StarterModule,
    InvoiceModule,
    ProfileModule,
    UserModule,
    ProductModule,
    InfiniteScrollModule,
    NewInvoiceModule

  ],
  providers: [
  ],
  entryComponents: [

  ],
  declarations: [
    ExpansionComponent,
  ]
})

export class ProtectedComponentsModule {}
